import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DealerService {

  constructor(private http: HttpClient) { }

  dealer_get(pageNo: string | number, stateId: string, cityId: string, areaId: string) {
    return this.http.get('dealers?page='+pageNo+'&&state_id='+stateId+'&&city_id='+cityId+'&&area_id='+areaId);
  }
  dealer_put(data: any, id: any):Observable<any> {
    return this.http.put('users/' + id, data);
  }
  dealer_delete(id: any) {
    return this.http.delete('dealers/' + id);
  }
  dealer_filter(pageNo: string | number, searchValue: any, stateId: string, cityId: string, areaId: string) {
    return this.http.get('dealers?page='+pageNo+'&&search_key='+searchValue+'&&state_id='+stateId+'&&city_id='+cityId+'&&area_id='+areaId);
  }
  dealer_print(data):Observable<any> {
    return this.http.post('get_dealers_info',data);
  }

  bank_get() {
    return this.http.get('banks');
  }
  year_get() {
    return this.http.get('passed_out_years');
  }
}
