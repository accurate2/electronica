import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BannerService {

  constructor(private http: HttpClient) { }

  banner_create(data: any):Observable<any> {
    return this.http.post('banners', data);
  }
  banner_get(pageNo) {
    console.log(pageNo)
    return this.http.get('banners?page='+pageNo);
  }
  banner_put(data: any, id: any):Observable<any> {
    return this.http.put('banners/' + id, data);
  }
  banner_delete(id: any) {
    return this.http.delete('banners/' + id);
  }
}
