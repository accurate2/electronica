import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  admin_create(data: any):Observable<any> {
    return this.http.post('users', data);
  }
  admin_get(pageNo: string | number) {
    return this.http.get('users?page='+pageNo);
  }
  admin_put(data: any, id: any):Observable<any> {
    return this.http.put('users/' + id, data);
  }
  admin_delete(id: any) {
    return this.http.delete('users/' + id);
  }
  admin_filter(pageNo: string | number, searchValue: any) {
    return this.http.get('users?page='+pageNo+'&&role=Admin'+'&&search_key='+searchValue);
  }
  role_get() {
    return this.http.get('roles?type=Staff');
  }
  admin_reset_pw(data: any):Observable<any> {
    return this.http.post('customize_password', data);
  }
}
