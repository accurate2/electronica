import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PriceService {

  constructor(private http: HttpClient) { }

  serviceType_create(data: any):Observable<any> {
    return this.http.post('service_types', data);
  }
  serviceType_get(pageNo) {
    return this.http.get('service_types?page='+pageNo);
  }
  serviceType_put(data: any, id: any):Observable<any> {
    return this.http.put('service_types/' + id, data);
  }
  serviceType_delete(id: any) {
    return this.http.delete('service_types/' + id);
  }
  serviceType_filter(pageNo,searchValue) {
    return this.http.get('service_types?page='+pageNo+'&&search_key='+searchValue);
  }
  
}


  
