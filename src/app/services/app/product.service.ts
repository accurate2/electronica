import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  //capacity

  capacity_create(data: any): Observable<any> {
    return this.http.post('capacities', data);
  }
  capacity_get(pageNo: string | number): Observable<any> {
    return this.http.get('capacities?page=' + pageNo);
  }
  capacity_put(data: any, id: any): Observable<any> {
    return this.http.put('capacities/' + id, data);
  }
  capacity_delete(id: any) {
    return this.http.delete('capacities/' + id);
  }
  capacity_filter(pageNo: string | number, searchValue) {
    return this.http.get('capacities?page=' + pageNo+'&&search_key='+searchValue);
  }

  //accuracies

  acceleration_create(data: any): Observable<any> {
    return this.http.post('accuracies', data);
  }
  acceleration_get(pageNo: string | number) {
    return this.http.get('accuracies?page=' + pageNo);
  }
  acceleration_put(data: any, id: any): Observable<any> {
    return this.http.put('accuracies/' + id, data);
  }
  acceleration_delete(id: any) {
    return this.http.delete('accuracies/' + id);
  }
  accuracies_getByCapacity(capacity): Observable<any> {
    return this.http.get('accuracies?capacity='+capacity);
  }
  acceleration_filter(pageNo: string | number, searchValue) {
    return this.http.get('accuracies?page=' + pageNo+'&&search_key='+searchValue);
  }
  

  //machine class

  machine_class_create(data: any): Observable<any> {
    return this.http.post('machine_classes', data);
  }
  machine_class_get(pageNo: string | number) {
    return this.http.get('machine_classes?page=' + pageNo);
  }
  machine_class_put(data: any, id: any): Observable<any> {
    return this.http.put('machine_classes/' + id, data);
  }
  machine_class_delete(id: any) {
    return this.http.delete('machine_classes/' + id);
  }
  machine_class_getByCapacity(capacity, accuracy) {
    return this.http.get('find_machine_class?capacity='+ capacity +'&&accuracy='+ accuracy);
  }
  machine_class_filter(pageNo: string | number, searchValue) {
    return this.http.get('machine_classes?page=' + pageNo+'&&search_key='+searchValue);
  }
}
