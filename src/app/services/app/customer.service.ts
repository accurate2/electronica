import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { staticViewQueryIds } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) { }

  customer_get(pageNo: string | number, dealer, stateId, cityId, areaId, siCircle) {
    return this.http.get('customers?page='+pageNo+'&&dealer_id='+dealer+
    '&&state_id='+stateId+'&&city_id='+cityId+'&&area_id='+areaId+'&&si_circle='+siCircle);
  }
  customer_put(data: any, id: any):Observable<any> {
    return this.http.put('users/' + id, data);
  }
  customer_delete(id: any) {
    return this.http.delete('customers/' + id);
  }

  machine_get(pageNo: string | number) {
    return this.http.get('machines?page='+pageNo);
  }

  generate_qrCode(id: string | number) {
    return this.http.get('generate_qrcode?id='+ id);
  }

  download_qrCode(data: { customer_ids: any[]; }):Observable<Blob> {
    return this.http.post('download_qrcode', data, {responseType: 'blob'});
  }
  
  machine_create(data: any):Observable<any> {
    return this.http.post('machines', data);
  }

  machine_put(data: any, id: any):Observable<any> {
    return this.http.put('machines/' + id, data);
  }

  customer_getById(id: string | number) {
    return this.http.get('customers/' + id);
  }
  machine_getById(id: string | number) {
    return this.http.get('machines/' + id);
  }
  image_upload(data: FormData):Observable<any> {
    return this.http.post('machine_assets', data);
  }
  sl_no_validation(sl_no): Observable<any> {
    return this.http.get('validate_serial_no?serial_no='+ sl_no );
  }
  customer_filter(pageNo: string | number, searchValue: any, dealer, stateId, cityId, areaId, siCircle) {
    return this.http.get('customers?page='+pageNo+'&&search_key='+searchValue+'&&dealer_id='+dealer+
    '&&state_id='+stateId+'&&city_id='+cityId+'&&area_id='+areaId+'&&si_circle='+siCircle);
  }

  categories_get() {
    return this.http.get('customer_categories');
  }
  sub_categories_get(categoryId) {
    return this.http.get('sub_categories?id='+categoryId);
  }
  sub_categories() {
    return this.http.get('sub_categories');
  }



}
