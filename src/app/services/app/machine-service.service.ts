import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MachineServiceService {

  constructor(private http: HttpClient) { }

  machine_service_get(pageNo: string | number, dealer, customer, status) {
    return this.http.get('machine_services?page='+pageNo+'&&dealer_id='+dealer+'&&customer_id='+customer+'&&status='+status);
  }
  machine_service_put(data: any, id: any):Observable<any> {
    return this.http.put('machine_services/' + id, data);
  }
  machine_service_delete(id: any) {
    return this.http.delete('machine_services/' + id);
  }

  machine_service_filter(pageNo: string | number, searchValue: any, dealer, customer, status) {
    return this.http.get('machine_services?page='+pageNo+'&&search_key='+searchValue+'&&dealer_id='+dealer+'&&customer_id='+customer+'&&status='+status);
  }
}
