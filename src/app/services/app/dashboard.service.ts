import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  admin_analytics(stamp_sd,stamp_ed,renew_sd,renew_ed,mach_sd,mach_ed,cus_sd,cus_ed,dealer_sd,dealer_ed,analytics_sd,analytics_ed) {
    return this.http.get('admin_analytics?tot_stamp_sd='+stamp_sd+'&&tot_stamp_ed='+stamp_ed
    +'&&renewal_sd='+renew_sd+'&&renewal_ed='+renew_ed+'&&tot_mach_sd='+mach_sd
    +'&&tot_mach_ed='+mach_ed+'&&tot_cus_sd='+cus_sd+'&&tot_cus_ed='+cus_ed
    +'&&tot_dealer_sd='+dealer_sd+'&&tot_dealer_ed='+dealer_ed+
    '&&cus_analytics_sd='+analytics_sd+'&&cus_analytics_ed='+analytics_ed);
  }
  service_analytics(startDate, endDate) {
    return this.http.get('service_status_analytics?start_date='+startDate+'&&end_date='+endDate);
  }
}
