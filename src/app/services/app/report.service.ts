import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: HttpClient) { }

  report_get(type, from, to, dealer, customer, city, service, model, si_circle) {
    return this.http.get('reports?report_type=' + type + '&&from_date=' + from + '&&to_date=' + to +'&&dealer_id=' + dealer 
    +'&&customer_id='+customer+'&&city_id='+city + '&&service_type_id='+service+'&&mac_model='+model+'&&si_circle=' + si_circle);
  }

  dealers_get(cityId?) {
    let city_id = cityId ? cityId : "";
    return this.http.get('dealer_list?city_id='+city_id);
  }
  si_circle_get(): Observable<any> {
    return this.http.get('si_circle_list');
  }

  customers_get(dealerId) {
    let dealer_id = dealerId ? dealerId : "";
    return this.http.get('customer_list?dealer_id='+dealer_id);
  }

  state_get() {
    return this.http.get('states');
  }
  city_get(stateId?) {
    let state_id = stateId ? stateId : "";
    return this.http.get('cities?state_id=' + state_id);
  }
  service_get() {
    return this.http.get('get_service_types');
  }
  area_get(stateId, cityId) {
    let state_id = stateId ? stateId : "";
    let city_id = cityId ? cityId : "";
    return this.http.get('areas?city_id=' + city_id + '&&state_id=' + state_id);
  }

  report_search(type, from, to, dealer, customer, city, service, model, si_circle, search) {
    return this.http.get('reports?report_type=' + type + '&&from_date=' + from + '&&to_date=' + to +
      '&&dealer_id='+ dealer +'&&customer_id='+customer+'&&city_id='+city+ '&&service_type_id='+service+'&&mac_model='+model+'&&si_circle=' + si_circle + '&&search_key=' + search);
  }

  model_get(): Observable <any> {
    return this.http.get('model_list');
  }
}