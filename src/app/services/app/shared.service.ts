import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SharedService {

    constructor() { }
    private subject = new Subject<any>();
    private data = new Subject<any>();
    currentData = this.data.asObservable()



    setLoaderShownProperty(isLoading: boolean): void {
        // console.log(isLoading);
        this.subject.next({ isLoading });
    }

    getLoaderShownProperty(): Observable<any> {
        return this.subject.asObservable();
    }
    updateMessage(item: boolean) {
        this.data.next(item);
    }
}


