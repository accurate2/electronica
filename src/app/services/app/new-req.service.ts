import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewReqService {

  constructor(private http: HttpClient) { }

  new_req_get(pageNo: string | number, customer, status, start, end) {
    return this.http.get('machine_requirements?page='+pageNo+'&&customer_id='+customer+'&&status='+status+'&&start_date='+start+'&&end_date='+end);
  }
  new_req_create(data: any):Observable<any> {
    return this.http.post('machine_requirements/', data);
  }
  new_req_put(data: any, id: any):Observable<any> {
    return this.http.put('machine_requirements/' + id, data);
  }
  new_req_delete(id: any) {
    return this.http.delete('machine_requirements/' + id);
  }
  new_req_filter(pageNo: string | number, searchValue: any, customer, status, start, end) {
    return this.http.get('machine_requirements?page='+pageNo+'&&search_key='+searchValue+'&&customer_id='+customer+'&&status='+status+'&&start_date='+start+'&&end_date='+end);
  }
}
