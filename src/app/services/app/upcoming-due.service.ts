import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UpcomingDueService {

  
  constructor(private http: HttpClient) { }

  dues_get(pageNo: string | number, dealer, customer, status, state, city, area, start, end, quarter) {
    return this.http.get('stamping_dues?page='+pageNo+'&&dealer_id='+dealer+'&&customer_id='+customer+'&&status='+status
    +'&&state_id='+state+'&&city_id='+city+'&&area_id='+area+'&&start_date='+start+'&&end_date='+end+'&&stamp_quarters='+quarter);
  }
  dues_put(data: any, id: any):Observable<any> {
    return this.http.put('stamping_dues/' + id, data);
  }
  dues_delete(id: any) {
    return this.http.delete('stamping_dues/' + id);
  }
  dues_filter(pageNo: string | number, searchValue: any, dealer, customer, status, state, city, area, start, end, quarter) {
    return this.http.get('stamping_dues?page='+pageNo+'&&search_key='+searchValue+'&&dealer_id='+dealer+'&&customer_id='+customer+'&&status='+status
    +'&&state_id='+state+'&&city_id='+city+'&&area_id='+area+'&&start_date='+start+'&&end_date='+end+'&&stamp_quarters='+quarter);
  }
}
