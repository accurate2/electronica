import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  reset_password(data: any): Observable<any> {
    return this.http.post('reset_password', data);
  }
  forgot_password(data): Observable<any> {
    return this.http.get('verify_username?username=' + data);
  }

  otp_verification(data): Observable<any> {
    return this.http.post('verify_code', data);
  }
  change_password(data: any): Observable<any> {
    return this.http.post('save_new_password', data);
  }
  logout() {
    localStorage.clear();
  }

  getUser(data: { old_mob_number: string; new_mob_number: string; }): Observable<any> {
    console.log(data);
    return this.http.get('verify_username?username=' + data.old_mob_number + '&&new_mobile_no=' + data.new_mob_number);
  }
  validateUsername(mob_number): Observable<any> {
    return this.http.get('validate_username?username=' + mob_number);
  }
  changeMobile(data: any, id: any):Observable<any> {
    return this.http.put('users/' + id, data);
  }
  send_sms(data: any): Observable<any> {
    return this.http.post('send_sms', data);
  }
  getDealerCusCount(): Observable<any> {
    return this.http.get('user_count');
  }
  getSms_credit(): Observable<any> {
    return this.http.get('check_sms_credit'); 
  }
}
