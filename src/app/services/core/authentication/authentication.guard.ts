import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Logger } from '../logger.service';
import { TokenService } from './token.service';

const log = new Logger('AuthenticationGuard');

@Injectable()
export class AuthenticationGuard implements CanActivate {
    constructor(private router: Router, private tokenService: TokenService) { }

    canActivate(route: ActivatedRouteSnapshot,  state: RouterStateSnapshot): boolean {
        if (this.tokenService.isTokenValidated() || state.url.includes('machine_info') ) {
            return true;
        }
        log.debug('Not authenticated, redirecting and adding redirect url...');
       this.router.navigateByUrl('');
        return false;
    }
}
