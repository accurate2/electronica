import { Injectable } from '@angular/core';

const tokenKey = 'jwtToken';

/**
 * Provides storage for authentication jwt token.
 */
@Injectable()
export class TokenService {
  private token: string | null = null;

  constructor() { 
    const savedToken = localStorage.getItem('token');
    if (savedToken) {
      this.token = savedToken;
    }
  }
    /**
   * Checks if the token is validated.
   * @return True  if the token is validated.
   */
  isTokenValidated(): boolean {
    return this.token !== null ? true : false;
  }

   /**
   * Gets the token.
   * @return The token or null if the user is not authenticated.
   */
  getEncodedToken(): string | null {
    return this.token;
  }

  /**
   * Sets the encoded token.
   * @param token in string format retrieved from server.
   */
 /**
   * Sets the encoded token.
   * @param schema in string format retrieved from server.
   */
  setToken(token?: string | null) {
    this.token = token || null;

    if (token) {
        localStorage.setItem(tokenKey, token);
    } else {
      localStorage.removeItem(tokenKey);
    }
  }
  getRole() {
    const role = localStorage.getItem('role');
    return role;
  }
  getName() {
    const name = localStorage.getItem('user_name');
    return name;
  }
}

