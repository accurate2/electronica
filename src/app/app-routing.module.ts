import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ShellComponent } from 'app/components/shell';
import { AuthenticationGuard as AuthGuard } from 'app/services/core';
import { PreloadingStrategyService } from 'core-services/preloading-strategy.service';


const routes: Routes = [
  { path: '', loadChildren: () => import('./components/login/login.module').then(m => m.LoginModule) },
  { path: 'forgot-password', loadChildren: () => import('./components/forget-password/forget-password.module').then(m => m.ForgetPasswordModule) },
  {
    path: '', component: ShellComponent,
    children: [
      { path: 'dashboard', loadChildren: () => import('./components/dashboard/dashboard.module').then(m => m.DashboardModule), data: { preload: true } },
      { path: 'staff', loadChildren: () => import('./components/admin/admin.module').then(m => m.AdminModule), data: { preload: true } },
      { path: 'banner', loadChildren: () => import('./components/banner/banner.module').then(m => m.BannerModule) },
      { path: 'dealer', loadChildren: () => import('app/components/dealer/dealer.module').then(m => m.DealerModule) },
      { path: 'customer', loadChildren: () => import('app/components/customer/customer.module').then(m => m.CustomerModule) },
      { path: 'service', loadChildren: () => import('app/components/machine-service/machine-service.module').then(m => m.MachineServiceModule) },
      { path: 'new-req', loadChildren: () => import('app/components/new-req/new-req.module').then(m => m.NewReqModule) },
      { path: 'report', loadChildren: () => import('app/components/report/report.module').then(m => m.ReportModule) },
      { path: 'price', loadChildren: () => import('app/components/price/price.module').then(m => m.PriceModule) },
      { path: 'sms', loadChildren: () => import('app/components/send-sms/send-sms.module').then(m => m.SendSmsModule) },
      { path: 'product', loadChildren: () => import('app/components/product/product.module').then(m => m.ProductModule) },
      { path: 'due', loadChildren: () => import('app/components/upcoming-due/upcoming-due.module').then(m => m.UpcomingDueModule) },
      { path: 'setting', loadChildren: () => import('app/components/setting/setting.module').then(m => m.SettingModule) },
      { path: 'reset-password', loadChildren: () => import('./components/reset-password/reset-password.module').then(m => m.ResetPasswordModule) },
      { path: 'masters', loadChildren: () => import('./components/masters/masters.module').then(m => m.MastersModule) },
    ],
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadingStrategyService, scrollPositionRestoration: 'enabled' }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
