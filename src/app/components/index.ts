export * from 'components/admin';
export * from 'components/banner'; 
export * from 'components/customer'; 
export * from 'components/dealer';
export * from 'components/login';
export * from 'components/new-req';
export * from 'components/price';
export * from 'components/product';
export * from 'components/report';
export * from 'components/send-sms';
export * from 'components/machine-service';
export * from 'components/shell';
export * from 'components/upcoming-due';
export * from 'components/dashboard';
export * from 'components/forget-password';
export * from 'components/reset-password';