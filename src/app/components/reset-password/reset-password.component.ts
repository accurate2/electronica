import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserService, SharedService } from 'services/app';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { Router } from '@angular/router';
import {MustMatch} from 'app/components/admin';



@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  resetForm: FormGroup;
  hide = true;
  hide1 = true;
  hide2 = true;
  clicked = false;

  constructor(private fb: FormBuilder, private toast: ToastrService, private user: UserService, public router: Router, private sharedService: SharedService) { }


  ngOnInit() {
    this.createResetForm();
  }

  createResetForm() {
    this.resetForm = this.fb.group({
      old_password: ['', Validators.required],
      password: ['', Validators.required],
      password_confirmation: ['', Validators.required],
    }, { validator: MustMatch('password', 'password_confirmation') });
  }

  submit() {
    Object.keys(this.resetForm.controls).forEach((key) => {
      console.log(typeof(this.resetForm.get(key).value))
      if(typeof(this.resetForm.get(key).value) == 'string') {
      this.resetForm.get(key).setValue(this.resetForm.get(key).value.trim())
      }
    });
    if (this.resetForm.invalid) {
      return;
    }
    this.clicked = true;
    let data;
    data = this.resetForm.value;
    data.user_id = localStorage.getItem('user_id');
    console.log(data);
    this.user.reset_password(data).pipe(untilDestroyed(this)).subscribe(res => {
      this.clicked = false;
      if(res.status) {
      this.toast.success(res.message);
      this.router.navigateByUrl('');
      this.ngOnInit();
      } else {
        this.toast.error(res.message);
      }
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      this.clicked = false;
      this.toast.error('Something Went Wrong');
    });

  }
  spaceNotallowed(event) {
    if (event.keyCode === 32 ) {
      return false;
    }
  }
  cancel() {
    this.router.navigateByUrl('/dashboard');
  }

  ngOnDestroy() { }

}

