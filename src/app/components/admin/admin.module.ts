import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { AdminComponent, admin_dialog_box, admin_pw_reset } from './admin.component';
import { SharedModule } from 'app/shared';


const routes: Routes = [{ path: '', component: AdminComponent }];

@NgModule({
  declarations: [AdminComponent, admin_dialog_box, admin_pw_reset],
  entryComponents: [ admin_dialog_box, admin_pw_reset ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminModule { }





