import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { AdminService, SharedService } from 'services/app';
import { untilDestroyed } from 'ngx-take-until-destroy';
export interface Admin_Dialog_Data { }
import Swal from 'sweetalert2';
import { TokenService } from 'app/services';



@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  users: any;
  pageNo: number;
  admin_scroll: any;
  searchValue = "";

  displayedColumns: string[] = ['position', 'id', 'full_name', 'username', 'contact_no', 'role', 'status', 'action'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(public dialog: MatDialog, private admin: AdminService, private toast: ToastrService, private sharedService: SharedService, public token: TokenService) { }

  ngOnInit() {
    this.sharedService.setLoaderShownProperty(true);
    this.getUsers();
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(admin_dialog_box, {
      width: 'auto',
      data: { new: 'new' }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getUsers();
    });
  }

  edit(user, id) {
    const dialogRef = this.dialog.open(admin_dialog_box, {
      width: 'auto',
      data: { edit_admin: user, admin_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getUsers();
    });
  }

  reset_pw(user, id) {
    const dialogRef = this.dialog.open(admin_pw_reset, {
      width: 'auto',
      data: { edit_admin: user, admin_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getUsers();
    });
  }

  delete(id) {
    Swal.fire({
      title: 'Are you sure want to delete?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.admin.admin_delete(id).pipe(untilDestroyed(this)).subscribe(res => {
          this.toast.success('Deleted Successfully');
          this.ngOnInit();
        }, error => {
          this.toast.error('Something Went Wrong');
        });
      }
    });
  }

  getUsers() {
    this.pageNo = 1;
    this.searchValue = "";
    this.admin.admin_get(this.pageNo).pipe(untilDestroyed(this)).subscribe(res => {
      this.users = res;
      console.log(this.users)
      this.dataSource = new MatTableDataSource(this.users);
      this.dataSource.sort = this.sort;
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }

  applyFilter() {
    this.pageNo = 1;
    if (this.searchValue.length) {
      this.admin.admin_filter(this.pageNo, this.searchValue).pipe(untilDestroyed(this)).subscribe(res => {
        this.users = res;
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.sort = this.sort;
      });
    } else {
      this.getUsers();
    }
  }

  onScroll() {
    this.pageNo = this.pageNo + 1;
    this.admin.admin_filter(this.pageNo, this.searchValue).subscribe(res => {
      this.admin_scroll = res;
      this.admin_scroll.map(item => this.users.push(item));
      this.dataSource = new MatTableDataSource(this.users);
      this.dataSource.sort = this.sort;
    })
  }

  status(event, id) {
    if (event.checked) {
      let data = {
        status: 'Active'
      }
      Swal.fire({
        title: 'Are you sure want to Activate?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          this.admin.admin_put(data, id).pipe(untilDestroyed(this)).subscribe(res => {
            this.ngOnInit();
            this.toast.success('Activated');
          }, error => {
            this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
          });
        } else {
          this.ngOnInit();
        }
      });

    } else {
      let data = {
        status: 'Inactive'
      }
      Swal.fire({
        title: 'Are you sure want to Deactivate?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          this.admin.admin_put(data, id).pipe(untilDestroyed(this)).subscribe(res => {
            this.ngOnInit();
            this.toast.warning('Deactivated');
          }, error => {
            this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
          });
        } else {
          this.ngOnInit();
        }
      });
    }
  }

  ngOnDestroy() { }
}

// admin edit page 

@Component({
  selector: 'admin_dialog_box',
  templateUrl: 'admin_dialog_box.html',
})

export class admin_dialog_box {

  adminForm: FormGroup;
  value: any;
  editMode = false;
  hide = true;
  hide1 = true;
  roles: any;
  clicked = false;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<admin_dialog_box>, @Inject(MAT_DIALOG_DATA) public data: Admin_Dialog_Data,
    private toast: ToastrService, private admin: AdminService) {
    this.value = this.data;
  }

  ngOnInit() {
    this.createAdminForm();
    this.getRoles();
  }
  createAdminForm() {
    if (this.value.new) {
      this.adminForm = this.fb.group({
        full_name: ['', Validators.required],
        username: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required],
        password_confirmation: ['', Validators.required],
        designation: [''],
        role_id: ['', Validators.required],
        contact_no: ['', Validators.required]
      }, { validator: MustMatch('password', 'password_confirmation') });
    } else {
      this.editMode = true;
      this.adminForm = this.fb.group({
        full_name: [this.value.edit_admin.full_name, Validators.required],
        username: [this.value.edit_admin.username, [Validators.required, Validators.email]],
        designation: [this.value.edit_admin.designation],
        role_id: [this.value.edit_admin.role.id, Validators.required],
        contact_no: [this.value.edit_admin.contact_no, Validators.required]
      });
    }
  }
  getRoles() {
    this.admin.role_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.roles = res;
    });
  }
  submit() {
    Object.keys(this.adminForm.controls).forEach((key) => {
      console.log(typeof(this.adminForm.get(key).value))
      if(typeof(this.adminForm.get(key).value) == 'string') {
      this.adminForm.get(key).setValue(this.adminForm.get(key).value.trim())
      }
    });
    let data = this.adminForm.value;
    console.log(data);
    data.distributor_id = localStorage.getItem('user_id');
    if (this.adminForm.invalid) {
      return;
    }
    this.clicked = true;
    if (this.value.new) {
      data.status = 'Active';
      this.admin.admin_create(data).pipe(untilDestroyed(this)).subscribe(res => {
        this.clicked = false;
        this.dialogRef.close();
        this.toast.success('Created Successfully');
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    } else {
      this.admin.admin_put(data, this.value.admin_id).pipe(untilDestroyed(this)).subscribe(res => {
        this.clicked = false;
        this.dialogRef.close();
        this.toast.success('Updated Successfully');
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    }
  }

  spaceNotallowed(event) {
    if (event.keyCode === 32) {
      return false;
    }
  }

  ngOnDestroy() { }
}
//admin password reset page 

@Component({
  selector: 'admin_pw-reset',
  templateUrl: 'admin_pw_reset.html',
})

export class admin_pw_reset {

  pw_resetForm: FormGroup;
  value: any;
  editMode = false;
  hide = true;
  hide1 = true;
  hide2 = true;
  roles: any;
  clicked = false;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<admin_dialog_box>, @Inject(MAT_DIALOG_DATA) public data: Admin_Dialog_Data,
    private toast: ToastrService, private admin: AdminService, public token: TokenService) {
    this.value = this.data;
    console.log(this.value)
  }

  ngOnInit() {
    this.createpwResetForm();
  }
  createpwResetForm() {
    this.pw_resetForm = this.fb.group({
      current_user_password: ['', Validators.required],
      password: ['', Validators.required],
      password_confirmation: ['', Validators.required],
    }, { validator: MustMatch('password', 'password_confirmation') });
  }

  submit() {
    Object.keys(this.pw_resetForm.controls).forEach((key) => {
      console.log(typeof(this.pw_resetForm.get(key).value))
      if(typeof(this.pw_resetForm.get(key).value) == 'string') {
      this.pw_resetForm.get(key).setValue(this.pw_resetForm.get(key).value.trim())
      }
    });
    let data = this.pw_resetForm.value;
    data.staff_id = this.value.admin_id;
    console.log(data);
    if (this.pw_resetForm.invalid) {
      return;
    }
    this.clicked = true;
    this.admin.admin_reset_pw(data).pipe(untilDestroyed(this)).subscribe(res => {
      this.clicked = false;
      if (res.status) {
        this.toast.success(res.message);
        this.dialogRef.close();
      } else {
        this.toast.error(res.message);
      }
    }, error => {
      this.clicked = false;
      this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
    });
  }

  spaceNotallowed(event) {
    if (event.keyCode === 32) {
      return false;
    }
  }

  ngOnDestroy() { }
}
export function MustMatch(new_password: string, confirm_password: string) {
  return (formGroup: FormGroup) => {
    const password = formGroup.controls[new_password];
    const confirmpass = formGroup.controls[confirm_password];
    if (confirmpass.errors && !confirmpass.errors.mustMatch) {
      return;
    }
    if (password.value !== confirmpass.value) {
      confirmpass.setErrors({ mustMatch: true });
    } else {
      confirmpass.setErrors(null);
    }
  }
}