import { Component, Input } from '@angular/core';
import { SharedService } from 'app/services';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})

export class LoaderComponent {
  @Input()isLoading=false;

  color = 'primary';
  mode = 'indeterminate';
}
