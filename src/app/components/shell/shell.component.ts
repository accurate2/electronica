import { Component, OnInit } from '@angular/core';
import { TokenService } from 'services/core';
import { SharedService } from 'services/app';


@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {

  constructor(public token: TokenService, private sharedService: SharedService) { }
  isLoading: boolean = false;

  openmode: boolean = true;
  navMode: string = 'side';

  ngOnInit() {
    this.sharedService.getLoaderShownProperty().subscribe(({ isLoading }) => {
      setTimeout(() => {
        this.isLoading = isLoading;
      }, 0);
    });

    if (window.innerWidth < 769) {
      this.navMode = 'over';
      this.openmode = false
    }
  }

}
