import { Component, OnInit } from '@angular/core';
import { TokenService, SharedService } from 'app/services';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  activePath =  false;
  constructor(public token: TokenService, private shared: SharedService, private router: Router) {
    console.log(this.router.url)

  }

  ngOnInit() {
    this.shared.currentData.subscribe(currentData => {
      this.activePath = currentData;
      console.log(this.activePath)
    })
    if (this.router.url.includes('customer')) {
      this.activePath = true;
    }
  }
  closeNavBar() {
    this.activePath = false;
      console.log(this.activePath)
  }
  customerSelect() {
    this.activePath = true;
  }

}
