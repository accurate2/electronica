import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';


import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ShellComponent } from './shell.component';
import {SharedModule} from 'app/shared';
import { LoaderComponent } from './loader/loader.component';




@NgModule({
  declarations: [ HeaderComponent, FooterComponent, SidebarComponent, ShellComponent, LoaderComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule
  ],
  exports: [
    HeaderComponent, FooterComponent, SidebarComponent, ShellComponent
  ]
})
export class ShellModule { }
