import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from 'app/services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public router: Router, public token: TokenService) { }

  @Output() toggleMenu = new EventEmitter()

  ngOnInit() {


    console.log(this.toggleMenu);
  }
  logout() {
    localStorage.clear();
    this.router.navigateByUrl('');
  }

  toggleEmit() {
    this.toggleMenu.emit(true)
  }

}
