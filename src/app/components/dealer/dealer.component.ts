import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort } from '@angular/material';
import { DealerService, SharedService, AdminService, ReportService } from 'services/app';
export interface Dealer_Dialog_Data { }
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { TokenService } from 'services/core';
import { environment } from 'environments/environment';
import { SelectionModel } from '@angular/cdk/collections';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import html2canvas from 'html2canvas';







@Component({
  selector: 'app-dealer',
  templateUrl: './dealer.component.html',
  styleUrls: ['./dealer.component.scss']
})
export class DealerComponent implements OnInit {

  dealers: any;
  pageNo: number;
  dealer_scroll: any;
  searchValue = "";
  panelOpenState = false;
  state = "";
  city = "";
  area = "";
  dealerID: any[] = [];
  states: any;
  cities: any;
  areas: any;
  state_list: any;
  city_list: any;
  area_list: any;
  stateId: any;
  cityId: any;
  print_option = true;
  list: any[] = [];
  print_list: any[] = [];
  state_input = "";
  city_input = "";
  area_input = "";



  displayedColumns: string[] = ['select', 'position', 'user.uuid', 'user.full_name', 'user.username', 'address_line1', 'city.name', 'area.name', 'user.status', 'activate', 'customer_count', 'action'];
  dataSource = new MatTableDataSource();
  selection = new SelectionModel(true, []);
  displayedColumns_print: string[] = ['key', 'value'];
  dataSource_print = new MatTableDataSource();


  @ViewChild(MatSort, { static: true }) sort: MatSort;

  sortingDataAccessor(item, property) {
    if (property.includes('.')) {
      return property.split('.')
        .reduce((object, key) => object[key], item);
    }
    return item[property];
  }

  toggle() {
    this.panelOpenState = !this.panelOpenState
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }



  constructor(public dialog: MatDialog, private dealer: DealerService, private toast: ToastrService, public token: TokenService, private sharedService: SharedService, private admin: AdminService, private report: ReportService) { }

  ngOnInit() {
    this.sharedService.setLoaderShownProperty(true);
    this.getDealers();
    this.getStates();
    this.getCities();
    this.getAreas();
    if (this.token.getRole() === 'Verifier') {
      this.displayedColumns = ['select', 'position', 'user.uuid', 'user.full_name', 'user.username', 'address_line1', 'city.name', 'area.name', 'user.status', 'activate', 'customer_count', 'action'];
    }

  }

  getDealers() {
    this.pageNo = 1;
    this.searchValue = "";
    this.dealer.dealer_get(this.pageNo, this.state, this.city, this.area).pipe(untilDestroyed(this)).subscribe(res => {
      this.dealers = res;
      console.log(this.dealers)
      this.dataSource = new MatTableDataSource(this.dealers);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }
  applyFilter() {
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.dealer.dealer_filter(this.pageNo, this.searchValue, this.state, this.city, this.area).pipe(untilDestroyed(this)).subscribe(res => {
        this.dealers = res;
        this.dataSource = new MatTableDataSource(this.dealers);
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.dataSource.sort = this.sort;
      });
    } else {
      this.getDealers();
    }
  }

  edit(user, id) {
    const dialogRef = this.dialog.open(dealer_dialog_box, {
      width: 'auto',
      data: { edit_dealer: user, dealer_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getDealers();
    });
  }

  delete(id) {
    Swal.fire({
      title: 'Are you sure want to delete?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.dealer.dealer_delete(id).pipe(untilDestroyed(this)).subscribe(res => {
          this.toast.success('Deleted Successfully');
          this.ngOnInit();
        }, error => {
          this.toast.error('Something Went Wrong');
        });
      }
    });
  }

  onScroll() {
    this.pageNo = this.pageNo + 1;
    this.dealer.dealer_filter(this.pageNo, this.searchValue, this.state, this.city, this.area).subscribe(res => {
      this.dealer_scroll = res;
      this.dealer_scroll.map(item => this.dealers.push(item));
      this.dataSource = new MatTableDataSource(this.dealers);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
    })
  }

  status(event, id) {
    if (event.checked) {
      let data = {
        status: 'Active'
      }
      Swal.fire({
        title: 'Are you sure want to Activate?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          this.admin.admin_put(data, id).pipe(untilDestroyed(this)).subscribe(res => {
            this.ngOnInit();
            this.toast.success('Activated');
          }, error => {
            this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
          });
        } else {
          this.ngOnInit();
        }
      });
    } else {
      let data = {
        status: 'Inactive'
      }
      Swal.fire({
        title: 'Are you sure want to Deactivate?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          this.admin.admin_put(data, id).pipe(untilDestroyed(this)).subscribe(res => {
            this.ngOnInit();
            this.toast.success('Deactivated');
          }, error => {
            this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
          });
        } else {
          this.ngOnInit();
        }
      });
    }
  }

  filter() {
    console.log(this.state, this.city, this.area);
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.dealer.dealer_filter(this.pageNo, this.searchValue, this.state, this.city, this.area).pipe(untilDestroyed(this)).subscribe(res => {
        this.dealers = res;
        this.dataSource = new MatTableDataSource(this.dealers);
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.dataSource.sort = this.sort;
      }, error => {
        this.toast.error('Something Went Wrong');
      });
    } else {
      this.getDealers();
    }
  }
  clear() {
    this.state = "";
    this.city = "";
    this.area = "";
    this.state_input = "";
    this.city_input = "";
    this.area_input = "";
    // this.cities = [];
    // this.areas = [];
    this.ngOnInit();
  }
  selectAll(event) {
    console.log(event);
    if (event.checked) {
      this.dealerID = [];
    }
    this.dealers.map(item => {
      this.selectDealer(item.id, event);
    });
  }

  selectDealer(dealer_id, event) {
    console.log(dealer_id, event.checked);
    if (event.checked) {
      this.dealerID.push(dealer_id);
    } else {
      const index: number = this.dealerID.indexOf(dealer_id);
      console.log(index);
      if (index > -1) {
        this.dealerID.splice(index, 1);
      }
    }
    console.log(this.dealerID);
  }
  getStates() {
    this.report.state_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.states = res;
      this.state_list = res;
    });
  }
  search_state(event) {
    let value = event.trim().toLowerCase();
    this.states = this.state_list.filter(state => state.name.toLowerCase().indexOf(value) > -1);
    if (this.states.length === 0) {
      this.state = "";
    }
  }
  getCities(event?) {
    this.city = "";
    this.area = "";
    this.city_input = "";
    this.area_input = "";
    this.stateId = event;
    this.report.city_get(this.stateId).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.cities = res;
      this.city_list = res;
    });
  }
  search_city(event) {
    let value = event.trim().toLowerCase();
    this.cities = this.city_list.filter(city => city.name.toLowerCase().indexOf(value) > -1);
    if (this.cities.length === 0) {
      this.city = "";
    }
  }
  getAreas(event?) {
    this.area = "";
    this.area_input = "";
    this.cityId = event;
    this.report.area_get(this.stateId, this.cityId).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.areas = res;
      this.area_list = res;
    });
  }
  search_area(event) {
    let value = event.trim().toLowerCase();
    this.areas = this.area_list.filter(area => area.name.toLowerCase().indexOf(value) > -1);
    if (this.areas.length === 0) {
      this.area = "";
    }
  }

  print(id?) {
    this.list = [];
    this.print_list = [];
    let data;
    if (id) {
      data = {
        ids: id
      }
    } else {
      data = {
        ids: this.dealerID
      }
    }
    console.log(this.dealerID)
    this.dealer.dealer_print(data).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      res.map(dealer => {
        this.list = [
          ['Id', dealer.id],
          ['Dealer Name', dealer.user.full_name],
          ['Father Name', dealer.father_name],
          ['State', dealer.state.name],
          ['City', dealer.city.name],
          ['Area', dealer.area.name],
          ['Address', dealer.address_line1],
          ['Pincode', dealer.pincode],
          ['Mobile Number', dealer.alt_mob_no ? dealer.alt_mob_no : '---'],
          ['Qualification', dealer.qualification],
          ['College / School', dealer.institute_name],
          ['PassedOut Year', dealer.passing_year],
          ['Vehicle Number', dealer.account_info.vehicle_no ? dealer.account_info.vehicle_no : '---'],
          ['Bank Name', dealer.account_info.bank.name],
          ['Account Number', dealer.account_info.account_no],
          ['Accountant Name', dealer.account_info.accountant_name],
          ['IFSC Code', dealer.account_info.ifsc_code],
          ['Branch Name', dealer.account_info.branch_name],
          ['Customer Count', dealer.customer_count],
          ['Status', dealer.user.status ? dealer.user.status : 'Not Verified'],
        ]
        this.print_list.push(this.list);
      });
      var doc = new jsPDF();
      var col = ["key", "Value"];
      console.log(this.print_list)
      for (let print of this.print_list) {
        console.log(print[1][1]);
        doc.autoTable(col, print, {
          theme: 'grid', headStyles: {
            fillColor: [128, 128, 128]
          },
          styles: {
            fontSize: 12,
          },
          margin: { bottom: 100 }
        });
        doc.text(70, 10, "Dealer Name" + ":" + print[1][1], 'left');
      }
      doc.setProperties({
        title: 'dealer.pdf'
      });

      // doc.autoPrint();
      window.open(doc.output('bloburl'));
      // doc.save('dealer.pdf');
    });
    this.dealerID = [];
    this.selection.clear();
  }

  ngOnDestroy() { }
}
@Component({
  selector: 'dealer_dialog_box',
  templateUrl: 'dealer_dialog_box.html',
})

export class dealer_dialog_box {

  value: any;
  regFee = 1000;
  BaseURL: string = environment.imageUrl;
  states: any;
  cities: any;
  banks: any;
  stateId: any;
  list: any[] = [];
  print_list: any[] = [];
  state_list: any;
  city_list: any;
  clicked = false;
  years: any;


  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<dealer_dialog_box>, @Inject(MAT_DIALOG_DATA) public data: Dealer_Dialog_Data, private dealer: DealerService, private toast: ToastrService, public token: TokenService, private report: ReportService) {
    this.value = this.data;
    console.log(this.value)
  }

  myControl = new FormControl();
  dealerForm: FormGroup;


  ngOnInit() {
    console.log(this.value.edit_dealer.user.reg_fee);
    if (this.value.edit_dealer.user.reg_fee == 0 || this.value.edit_dealer.user.reg_fee == null) {
      this.value.edit_dealer.user.reg_fee = this.regFee;
    }
    this.createDealerForm();
    this.getStates();
    this.getCities(this.value.edit_dealer.state.id);
    this.getBanks();
    this.getYears();
  }

  createDealerForm() {
    this.dealerForm = this.fb.group({
      full_name: [this.value.edit_dealer.user.full_name, Validators.required],
      uuid: [this.value.edit_dealer.user.uuid],
      status: [this.value.edit_dealer.user.status],
      username: [this.value.edit_dealer.user.username],
      dealer_attributes: this.fb.group({
        id: [this.value.edit_dealer.id],
        father_name: [this.value.edit_dealer.father_name, Validators.required],
        aadhar_no: [this.value.edit_dealer.aadhar_no],
        address_line1: [this.value.edit_dealer.address_line1, Validators.required],
        state: [this.value.edit_dealer.state.id, Validators.required],
        city: [this.value.edit_dealer.city.id, Validators.required],
        area: [this.value.edit_dealer.area.id, Validators.required],
        pincode: [this.value.edit_dealer.pincode, Validators.required],
        qualification: [this.value.edit_dealer.qualification, Validators.required],
        institute_name: [this.value.edit_dealer.institute_name, Validators.required],
        passing_year: [parseInt(this.value.edit_dealer.passing_year), Validators.required],
        account_info_attributes: this.fb.group({
          id: [this.value.edit_dealer.account_info.id],
          vehicle_no: [this.value.edit_dealer.account_info.vehicle_no],
          insurance_url: [this.value.edit_dealer.account_info.insurance_url],
          bank_name: [this.value.edit_dealer.account_info.bank.id, Validators.required],
          accountant_name: [this.value.edit_dealer.account_info.accountant_name, Validators.required],
          account_no: [this.value.edit_dealer.account_info.account_no, Validators.compose([Validators.required,
          Validators.pattern('[0-9]{9,18}')])],
          ifsc_code: [this.value.edit_dealer.account_info.ifsc_code, Validators.compose([Validators.required,
          Validators.pattern('^[A-Za-z]{4}[a-zA-Z0-9]{7}$')])],
          branch_name: [this.value.edit_dealer.account_info.branch_name, Validators.required],
          pa_policy_no: [this.value.edit_dealer.account_info.pa_policy_no],
          reg_fee: [this.value.edit_dealer.user.reg_fee, Validators.required]
        })
      }),
    })
  }
  getStates() {
    this.report.state_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.states = res;
      this.state_list = res;
    });
  }
  search_state(event) {
    let value = event.trim().toLowerCase();
    this.states = this.state_list.filter(state => state.name.toLowerCase().indexOf(value) > -1);
    if (this.states.length === 0) {
      this.dealerForm.patchValue({
        dealer_attributes: ({
          state: ""
        })
      })
    }
  }

  getCities(event) {
    this.stateId = event;
    this.report.city_get(this.stateId).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.cities = res;
      this.city_list = res;
    });
  }
  search_city(event) {
    let value = event.trim().toLowerCase();
    this.cities = this.city_list.filter(city => city.name.toLowerCase().indexOf(value) > -1);
    if (this.cities.length === 0) {
      this.dealerForm.patchValue({
        dealer_attributes: ({
          city: ""
        })
      })
    }
  }

  getBanks() {
    this.dealer.bank_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.banks = res;
    });
  }
  getYears() {
    this.dealer.year_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.years = res;
    });
  }

  submit() {
    Object.keys(this.dealerForm.controls).forEach((key) => {
      console.log(typeof(this.dealerForm.get(key).value))
      if(typeof(this.dealerForm.get(key).value) == 'string' ) {
      this.dealerForm.get(key).setValue(this.dealerForm.get(key).value.trim())
      } 
    });
    Object.keys(this.dealerForm.value.dealer_attributes).forEach((key) => {
      console.log(typeof(this.dealerForm.controls.dealer_attributes.get(key).value))
      if(typeof(this.dealerForm.controls.dealer_attributes.get(key).value) == 'string' ) {
        this.dealerForm.controls.dealer_attributes.get(key).setValue(this.dealerForm.controls.dealer_attributes.get(key).value.trim())
      } 
    });
    Object.keys(this.dealerForm.value.dealer_attributes.account_info_attributes).forEach((key) => {
      console.log(this.dealerForm.controls.dealer_attributes.get('account_info_attributes').get(key).value)
      console.log(typeof(this.dealerForm.controls.dealer_attributes.get('account_info_attributes').get(key).value))
      if(typeof(this.dealerForm.controls.dealer_attributes.get('account_info_attributes').get(key).value) == 'string' ) {
        this.dealerForm.controls.dealer_attributes.get('account_info_attributes').get(key).setValue(this.dealerForm.controls.dealer_attributes.get('account_info_attributes').get(key).value.trim())
      } 
    });
    let fee = this.dealerForm.value.dealer_attributes.account_info_attributes.reg_fee;
    let data = this.dealerForm.value;
    data.reg_fee = fee;
    console.log(this.dealerForm.value.status);
    if (this.value.edit_dealer.user.status === null) {
      if (this.dealerForm.value.status) {
        data.status = "Verified";
      } else {
        data.status = null;
      }
    }
    if (this.dealerForm.invalid) {
      // this.toast.info('Please Enter All Fields');
      return;
    }
    this.clicked = true;
    if (this.value.edit_dealer.user.status === data.status) {
      console.log(data);
      this.dealer.dealer_put(data, this.value.dealer_id).pipe(untilDestroyed(this)).subscribe(res => {
        // this.dialogRef.close();
        // this.ngOnInit();
        this.clicked = false;
        this.print();
        this.toast.success('Updated Successfully');
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    } else {
      Swal.fire({
        title: 'Are you sure to verify this dealer?',
        text: "You will not be able to modify this once you have done!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          this.dealer.dealer_put(data, this.value.dealer_id).pipe(untilDestroyed(this)).subscribe(res => {
            // this.dialogRef.close();
            // this.ngOnInit();
            this.clicked = false;
            this.print();
            this.toast.success('Updated Successfully');
          }, error => {
            this.clicked = false;
            this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
          });
        }
      });
    }
  }
  print() {
    let data = {
      ids: this.value.edit_dealer.id
    }
    console.log(data);
    this.dealer.dealer_print(data).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      res.map(dealer => {
        this.list = [
          ['Id', dealer.id],
          ['Dealer Name', dealer.user.full_name],
          ['Father Name', dealer.father_name],
          ['State', dealer.state.name],
          ['City', dealer.city.name],
          ['Area', dealer.area.name],
          ['Address', dealer.address_line1],
          ['Pincode', dealer.pincode],
          ['Mobile Number', dealer.alt_mob_no ? dealer.alt_mob_no : '---'],
          ['Qualification', dealer.qualification],
          ['College / School', dealer.institute_name],
          ['PassedOut Year', dealer.passing_year],
          ['Vehicle Number', dealer.account_info.vehicle_no ? dealer.account_info.vehicle_no : '---'],
          ['Bank Name', dealer.account_info.bank.name],
          ['Account Number', dealer.account_info.account_no],
          ['IFSC Code', dealer.account_info.ifsc_code],
          ['Branch Name', dealer.account_info.branch_name],
          ['Customer Count', dealer.customer_count],
          ['Status', dealer.user.status ? dealer.user.status : 'Not Verified'],
        ]
        this.print_list.push(this.list);
      });
      var doc = new jsPDF();
      var col = ["key", "Value"];
      for (let print of this.print_list) {
        console.log(print[1][1]);
        doc.autoTable(col, print, {
          theme: 'grid', headStyles: {
            fillColor: [128, 128, 128]
          },
          margin: { bottom: 130 }
        });
        doc.text(80, 10, "Dealer Name" + ":" + print[1][1], 'left');
      }
      doc.setProperties({
        title: 'dealer.pdf'
      });
      // doc.autoPrint();
      this.dialogRef.close();
      window.open(doc.output('bloburl'));
      // doc.save('dealer.pdf');
    });


  }
  ngOnDestroy() { }
}