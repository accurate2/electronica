import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { DealerComponent, dealer_dialog_box } from './dealer.component';
import { SharedModule } from 'app/shared';

const routes: Routes = [{ path: '', component: DealerComponent }];

@NgModule({
  declarations: [DealerComponent, dealer_dialog_box],
  entryComponents: [dealer_dialog_box],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]

})
export class DealerModule { }
