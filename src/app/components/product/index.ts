import { machine_class_dialog_box } from './product.component';

export * from './product.module';
export * from './product.component';
export {machine_class_dialog_box} from './product.component';
