import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
export interface Capacity_Dialog_Data { }
export interface Acceleration_Dialog_Data { }
export interface Machine_Class_Dialog_Data { }
export interface Service_Type_Dialog_Data { }
import { ProductService, PriceService, SharedService, CustomerService } from 'services/app';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { TokenService } from 'services/core';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  pageNo: number;
  capacity: any;
  acceleration: any;
  machine_class: any;
  service_type: any;
  capacity_scroll: any;
  acceleration_scroll: any;
  machine_scroll: any;
  service_type_scroll: any;
  searchValue = "";
  categories: any;
  sub_categories: any;

  capacity_displayedColumns: string[] = ['position', 'value', 'action'];
  capacity_dataSource = new MatTableDataSource();
  acceleration_displayedColumns: string[] = ['position', 'value', 'action'];
  acceleration_dataSource = new MatTableDataSource();
  machine_class_displayedColumns: string[] = ['position', 'capacity', 'accuracy', 'min_value', 'class_name', 'action'];
  machine_class_dataSource = new MatTableDataSource();
  service_type_displayedColumns: string[] = ['position', 'type_name', 'action'];
  service_type_dataSource = new MatTableDataSource();
  category_dataSource = new MatTableDataSource();
  category_displayedColumns: string[] = ['position', 'name'];
  sub_category_dataSource = new MatTableDataSource();
  sub_category_displayedColumns: string[] = ['position', 'name'];

  @ViewChild('capacity_sort', { static: true }) capacity_sort: MatSort;
  @ViewChild('accuracy_sort', { static: true }) accuracy_sort: MatSort;
  @ViewChild('machine_class_sort', { static: true }) machine_class_sort: MatSort;
  @ViewChild('service_type_sort', { static: true }) service_type_sort: MatSort;
  @ViewChild('category_sort', { static: true }) category_sort: MatSort;
  @ViewChild('sub_category_sort', { static: true }) sub_category_sort: MatSort;



  sortingDataAccessor(item, property) {
    if (property.includes('.')) {
      return property.split('.')
        .reduce((object, key) => object[key], item);
    }
    return item[property];
  }


  constructor(public dialog: MatDialog, private product: ProductService, private price: PriceService, private toast: ToastrService, public token: TokenService, private sharedService: SharedService, private customer: CustomerService) { }

  ngOnInit() {
    this.sharedService.setLoaderShownProperty(true);
    this.getMachineClass();
    if (this.token.getRole() === 'Verifier') {
      this.capacity_displayedColumns = ['position', 'value'];
      this.acceleration_displayedColumns = ['position', 'value'];
      this.machine_class_displayedColumns = ['position', 'capacity', 'accuracy', 'min_value', 'class_name',];
      this.service_type_displayedColumns = ['position', 'type_name'];
    }
  }
  select(tabGroup: { index: number }) {
    if (tabGroup.index === 0) {
      this.getMachineClass();
    } else if (tabGroup.index === 1) {
      this.getServiceTypes();
    } else if (tabGroup.index === 2) {
      this.getCategories();
    } else {
      this.getSubCategories();
    }
  }

  //machine class
  machineDialog(): void {
    const dialogRef = this.dialog.open(machine_class_dialog_box, {
      width: 'auto',
      data: { new: 'new' }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getMachineClass();
    });
  }

  editMachineClass(machine_class, id) {
    const dialogRef = this.dialog.open(machine_class_dialog_box, {
      width: 'auto',
      data: { edit_machine_class: machine_class, machine_class_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getMachineClass();
    });
  }

  getMachineClass() {
    this.pageNo = 1;
    this.searchValue = "";
    this.product.machine_class_get(this.pageNo).pipe(untilDestroyed(this)).subscribe(res => {
      this.machine_class = res;
      this.machine_class_dataSource = new MatTableDataSource(this.machine_class);
      this.machine_class_dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.machine_class_dataSource.sort = this.machine_class_sort;
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }
  applyFilter_machine_class() {
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.product.machine_class_filter(this.pageNo, this.searchValue).pipe(untilDestroyed(this)).subscribe(res => {
        this.machine_class = res;
        this.machine_class_dataSource = new MatTableDataSource(this.machine_class);
        this.machine_class_dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.machine_class_dataSource.sort = this.machine_class_sort;
      });
    } else {
      this.getMachineClass();
    }
  }

  deleteMachineClass(id) {
    Swal.fire({
      title: 'Are you sure want to delete?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.product.machine_class_delete(id).pipe(untilDestroyed(this)).subscribe(res => {
          this.toast.success('Deleted Successfully');
          this.getMachineClass();
        }, error => {
          this.toast.error('Something Went Wrong');
        });
      }
    });
  }
  onScrollMachine() {
    this.pageNo = this.pageNo + 1;
    console.log(this.pageNo)
    this.product.machine_class_filter(this.pageNo, this.searchValue).subscribe(res => {
      this.machine_scroll = res;
      this.machine_scroll.map(item => this.machine_class.push(item));
      this.machine_class_dataSource = new MatTableDataSource(this.machine_class)
      this.machine_class_dataSource.sort = this.machine_class_sort;
    });
  }

  //service Type

  serviceDialog(): void {
    const dialogRef = this.dialog.open(service_type_dialog_box, {
      width: 'auto',
      data: { new: 'new' }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getServiceTypes();
    });
  }

  editServiceType(service, id) {
    const dialogRef = this.dialog.open(service_type_dialog_box, {
      width: 'auto',
      data: { edit_service: service, service_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getServiceTypes();
    });
  }

  getServiceTypes() {
    this.pageNo = 1;
    this.searchValue = "";
    this.price.serviceType_get(this.pageNo).pipe(untilDestroyed(this)).subscribe(res => {
      this.service_type = res;
      this.service_type_dataSource = new MatTableDataSource(this.service_type);
      this.service_type_dataSource.sort = this.service_type_sort;
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }
  applyFilter_service_type() {
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.price.serviceType_filter(this.pageNo, this.searchValue).pipe(untilDestroyed(this)).subscribe(res => {
        this.service_type = res;
        this.service_type_dataSource = new MatTableDataSource(this.service_type);
        this.service_type_dataSource.sort = this.service_type_sort;
      });
    } else {
      this.getServiceTypes();
    }
  }

  deleteServiceType(id) {
    Swal.fire({
      title: 'Are you sure want to delete?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.price.serviceType_delete(id).pipe(untilDestroyed(this)).subscribe(res => {
          this.toast.success('Deleted Successfully');
          this.getServiceTypes();
        }, error => {
          this.toast.error('Something Went Wrong');
        });
      }
    });
  }
  onScrollService() {
    this.pageNo = this.pageNo + 1;
    this.price.serviceType_filter(this.pageNo, this.searchValue).subscribe(res => {
      this.service_type_scroll = res;
      this.service_type_scroll.map(item => this.service_type.push(item));
      this.service_type_dataSource = new MatTableDataSource(this.service_type)
      this.service_type_dataSource.sort = this.service_type_sort;
    });
  }

  getCategories() {
    this.customer.categories_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.categories = res;
      this.category_dataSource = new MatTableDataSource(this.categories);
      this.category_dataSource.sort = this.category_sort;
    });
  }
  getSubCategories() {
    this.customer.sub_categories().pipe(untilDestroyed(this)).subscribe(res => {
      this.sub_categories = res;
      this.sub_category_dataSource = new MatTableDataSource(this.sub_categories);
      this.sub_category_dataSource.sort = this.sub_category_sort;
    });
  }
  ngOnDestroy() { }
}
@Component({
  selector: 'capacity_dialog_box',
  templateUrl: 'capacity_dialog_box.html',
})

export class capacity_dialog_box {

  constructor() { }

  ngOnInit() {
  }
}

@Component({
  selector: 'acceleration_dialog_box',
  templateUrl: 'acceleration_dialog_box.html',
})

export class acceleration_dialog_box {

  constructor() { }

  ngOnInit() {
  }

}

@Component({
  selector: 'machine_class_dialog_box',
  templateUrl: 'machine_class_dialog_box.html',
})

export class machine_class_dialog_box {

  machineClassForm: FormGroup;
  value: any;
  clicked = false;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<machine_class_dialog_box>,
    @Inject(MAT_DIALOG_DATA) public data: Machine_Class_Dialog_Data, private product: ProductService, private toast: ToastrService) {
    this.value = data;
  }
  ngOnInit() {
    this.createMachineForm();
  }
  createMachineForm() {
    if (this.value.new) {
      this.machineClassForm = this.fb.group({
        class_name: ['', Validators.required],
        capacity: ['', Validators.required],
        accuracy: ['', Validators.required],
        min_value: ['', Validators.required],
        stamping_cost: [0]
      });
    } else {
      this.machineClassForm = this.fb.group({
        class_name: [this.value.edit_machine_class.class_name, Validators.required],
        capacity: [this.value.edit_machine_class.capacity, Validators.required],
        accuracy: [this.value.edit_machine_class.accuracy, Validators.required],
        min_value: [this.value.edit_machine_class.min_value, Validators.required]
      });
    }
  }

  submit() {
    Object.keys(this.machineClassForm.controls).forEach((key) => {
      console.log(typeof(this.machineClassForm.get(key).value))
      if(typeof(this.machineClassForm.get(key).value) == 'string') {
      this.machineClassForm.get(key).setValue(this.machineClassForm.get(key).value.trim())
      }
    });
    let data = this.machineClassForm.value;
    data.created_by_id = localStorage.getItem('user_id');
    if (this.machineClassForm.invalid) {
      // this.toast.info('Please Enter All Fields');
      return;
    }
    this.clicked = true;
    console.log(data);
    if (this.value.new) {
      this.product.machine_class_create(data).pipe(untilDestroyed(this)).subscribe(res => {
        this.clicked = false;
        this.dialogRef.close();
        this.toast.success('Created Successfully');
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    } else {
      this.product.machine_class_put(data, this.value.machine_class_id).pipe(untilDestroyed(this)).subscribe(res => {
        this.clicked = false;
        this.dialogRef.close();
        this.toast.success('Updated Successfully');
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    }
  }
  ngOnDestroy() { }
}

@Component({
  selector: 'service_type_dialog_box',
  templateUrl: 'service_type_dialog_box.html',
})

export class service_type_dialog_box {

  serviceForm: FormGroup;
  value: any;
  clicked = false;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<service_type_dialog_box>,
    @Inject(MAT_DIALOG_DATA) public data: Service_Type_Dialog_Data, private price: PriceService, private toast: ToastrService) {
    this.value = data;
  }
  ngOnInit() {
    this.createServiceForm();
  }
  createServiceForm() {
    if (this.value.new) {
      this.serviceForm = this.fb.group({
        type_name: ['', Validators.required],
        cost: [0]
      });
    } else {
      this.serviceForm = this.fb.group({
        type_name: [this.value.edit_service.type_name, Validators.required]
      });
    }
  }
  submit() {
    Object.keys(this.serviceForm.controls).forEach((key) => {
      console.log(typeof(this.serviceForm.get(key).value))
      if(typeof(this.serviceForm.get(key).value) == 'string') {
      this.serviceForm.get(key).setValue(this.serviceForm.get(key).value.trim())
      }
    });
    let data = this.serviceForm.value;
    data.created_by_id = localStorage.getItem('user_id');
    if (this.serviceForm.invalid) {
      this.toast.info('Please Enter Service Type');
      return;
    }
    this.clicked = true;
    if (this.value.new) {
      this.price.serviceType_create(data).pipe(untilDestroyed(this)).subscribe(res => {
        this.clicked = false;
        this.dialogRef.close();
        this.toast.success('Created Successfully');
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    } else {
      this.price.serviceType_put(data, this.value.service_id).pipe(untilDestroyed(this)).subscribe(res => {
        this.clicked = false;
        this.dialogRef.close();
        this.toast.success('Updated Successfully');
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    }
  }
  ngOnDestroy() { }
}