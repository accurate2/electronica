import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { ProductComponent, capacity_dialog_box, acceleration_dialog_box, machine_class_dialog_box, service_type_dialog_box } from './product.component';
import { SharedModule } from 'app/shared';

const routes: Routes = [{ path: '', component: ProductComponent }];

@NgModule({
  declarations: [ProductComponent, capacity_dialog_box, acceleration_dialog_box, machine_class_dialog_box, service_type_dialog_box],
  entryComponents: [capacity_dialog_box, acceleration_dialog_box, machine_class_dialog_box, service_type_dialog_box],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ProductModule { }
