import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort } from '@angular/material';
export interface Upcoming_Due_Dialog_Data { }
import { UpcomingDueService, ProductService, SharedService, ReportService, CustomerService } from 'services/app';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { TokenService } from 'services/core';
import { environment } from 'environments/environment';
import { DatePipe } from '@angular/common';



@Component({
  selector: 'app-upcoming-due',
  templateUrl: './upcoming-due.component.html',
  styleUrls: ['./upcoming-due.component.scss']
})
export class UpcomingDueComponent implements OnInit {

  dues: any;
  pageNo: number;
  dues_scroll: any;
  searchValue = "";
  dealers: any;
  dealer = "";
  customers: any;
  customer = "";
  status = "";
  panelOpenState = false;
  states: any;
  cities: any;
  areas: any;
  stateId: any;
  cityId: any;
  state = "";
  city = "";
  area = "";
  due_date = "";
  due_date_begin = "";
  due_date_end = "";
  quarter = "";
  dealer_list: any;
  customers_list: any;
  state_list: any;
  city_list: any;
  area_list: any;
  state_input = "";
  city_input = "";
  area_input = "";
  dealer_input = "";
  customer_input = "";


  displayedColumns: string[] = ['position', 'customer.customer_name', 'customer.area.name', 'customer.city.name', 'machine.si_circle', 'stamp_due_date', 'machine.stamp_quarters', 'verified_at', 'status', 'action'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  sortingDataAccessor(item, property) {
    if (property.includes('.')) {
      return property.split('.')
        .reduce((object, key) => object[key], item);
    }
    return item[property];
  }
  toggle() {
    this.panelOpenState = !this.panelOpenState
  }

  constructor(public dialog: MatDialog, private due: UpcomingDueService, private toast: ToastrService, public token: TokenService, private sharedService: SharedService, private report: ReportService, public datepipe: DatePipe) { }

  ngOnInit() {
    this.sharedService.setLoaderShownProperty(true);
    this.getDues();
    if (this.token.getRole() === 'Verifier') {
      this.displayedColumns = ['position', 'customer.customer_name', 'customer.area.name', 'customer.city.name', 'machine.si_circle', 'stamp_due_date', 'machine.stamp_quarters', 'verified_at', 'status'];
    }
    this.getDealers();
    this.getCustomers();
    this.getStates();
    this.getCities();
    this.getAreas();
  }

  edit(user, id) {
    const dialogRef = this.dialog.open(upcoming_due_dialog_box, {
      width: 'auto',
      data: { edit_due: user, due_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getDues();
    });
  }


  getDues() {
    this.pageNo = 1;
    this.searchValue = "";
    this.due.dues_get(this.pageNo, this.dealer, this.customer, this.status, this.state, this.city, this.area, this.due_date_begin, this.due_date_end, this.quarter).pipe(untilDestroyed(this)).subscribe(res => {
      this.dues = res;
      this.dataSource = new MatTableDataSource(this.dues);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }
  applyFilter() {
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.due.dues_filter(this.pageNo, this.searchValue, this.dealer, this.customer, this.status, this.state, this.city, this.area, this.due_date_begin, this.due_date_end, this.quarter).pipe(untilDestroyed(this)).subscribe(res => {
        this.dues = res;
        this.dataSource = new MatTableDataSource(this.dues);
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.dataSource.sort = this.sort;
      });
    } else {
      this.getDues();
    }
  }

  onScroll() {
    this.pageNo = this.pageNo + 1;
    this.due.dues_filter(this.pageNo, this.searchValue, this.dealer, this.customer, this.status, this.state, this.city, this.area, this.due_date_begin, this.due_date_end, this.quarter).subscribe(res => {
      this.dues_scroll = res;
      this.dues_scroll.map(item => this.dues.push(item));
      this.dataSource = new MatTableDataSource(this.dues);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
    });
  }

  delete(id) {
    Swal.fire({
      title: 'Are you sure want to delete?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.due.dues_delete(id).pipe(untilDestroyed(this)).subscribe(res => {
          this.toast.success('Deleted Successfully');
          this.ngOnInit();
        }, error => {
          this.toast.error('Something Went Wrong');
        });
      }
    });
  }
  getDealers() {
    this.report.dealers_get().pipe(untilDestroyed(this)).subscribe(res => {
      this.dealers = res;
      this.dealer_list = res;
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_dealer(event) {
    let value = event.trim().toLowerCase();
    this.dealers = this.dealer_list.filter(dealer => dealer.full_name.toLowerCase().indexOf(value) > -1);
    if (this.dealers.length === 0) {
      this.dealer = "";
    }
  }
  getCustomers(event?) {
    this.customer = "";
    this.customer_input = "";
    this.report.customers_get(event).pipe(untilDestroyed(this)).subscribe(res => {
      this.customers = res;
      this.customers_list = res;
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_customer(event) {
    let value = event.trim().toLowerCase();
    this.customers = this.customers_list.filter(customer => customer.full_name.toLowerCase().indexOf(value) > -1);
    if (this.customers.length === 0) {
      this.customer = "";
    }
  }
  getStates() {
    this.report.state_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.states = res;
      this.state_list = res;
    });
  }
  search_state(event) {
    let value = event.trim().toLowerCase();
    this.states = this.state_list.filter(state => state.name.toLowerCase().indexOf(value) > -1);
    if (this.states.length === 0) {
      this.state = "";
    }
  }
  getCities(event?) {
    this.city = "";
    this.area = "";
    this.city_input = "";
    this.area_input = "";
    this.stateId = event;
    this.report.city_get(this.stateId).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.cities = res;
      this.city_list = res;
    });
  }
  search_city(event) {
    let value = event.trim().toLowerCase();
    this.cities = this.city_list.filter(city => city.name.toLowerCase().indexOf(value) > -1);
    if (this.cities.length === 0) {
      this.city = "";
    }
  }
  getAreas(event?) {
    this.area = "";
    this.area_input = "";
    this.cityId = event;
    this.report.area_get(this.stateId, this.cityId).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.areas = res;
      this.area_list = res;
    });
  }
  search_area(event) {
    let value = event.trim().toLowerCase();
    this.areas = this.area_list.filter(area => area.name.toLowerCase().indexOf(value) > -1);
    if (this.areas.length === 0) {
      this.area = "";
    }
  }

  dueDate(event) {
    console.log(event);
    this.due_date_begin = this.datepipe.transform(event.begin, 'yyyy-MM-dd');
    this.due_date_end = this.datepipe.transform(event.end, 'yyyy-MM-dd');
  }

  filter() {
    console.log(this.dealer, this.customer);
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.due.dues_filter(this.pageNo, this.searchValue, this.dealer, this.customer, this.status, this.state, this.city, this.area, this.due_date_begin, this.due_date_end, this.quarter).pipe(untilDestroyed(this)).subscribe(res => {
        this.dues = res;
        this.dataSource = new MatTableDataSource(this.dues);
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.dataSource.sort = this.sort;
        this.sharedService.setLoaderShownProperty(false);
      }, error => {
        this.toast.error('Something Went Wrong');
      });
    } else {
      this.getDues();
    }
  }
  clear() {
    this.dealer = "";
    this.customer = "";
    this.status = "";
    this.state = "";
    this.city = "";
    this.area = "";
    this.due_date = "";
    this.due_date_begin = "";
    this.due_date_end = "";
    this.quarter = "";
    this.state_input = "";
    this.city_input = "";
    this.area_input = "";
    this.dealer_input = "";
    this.customer_input = "";
    this.ngOnInit();
  }
  ngOnDestroy() { }
}

@Component({
  selector: 'upcoming_due_dialog_box',
  templateUrl: './upcoming_due_dialog_box.html',
  styleUrls: ['./upcoming-due.component.scss']
})

export class upcoming_due_dialog_box {

  dueForm: FormGroup;
  value: any;
  pageNo: any;
  success = false;
  searchValue: any;
  minDate = new Date();
  image: any;
  url: any;
  BaseURL: string = environment.imageUrl;
  clicked = false;


  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<upcoming_due_dialog_box>, @Inject(MAT_DIALOG_DATA) public data: Upcoming_Due_Dialog_Data, private product: ProductService, private toast: ToastrService, private due: UpcomingDueService, public token: TokenService, private customer: CustomerService) {
    this.value = this.data;
    console.log(this.value);
    console.log(this.value.edit_due.stamp_due_date);
    var d = new Date(this.value.edit_due.stamp_due_date);
    d.setMonth(d.getMonth() + 3);
    var date = d.toISOString().slice(0, 10);
    console.log(date);
    if (this.value.edit_due.payment_method === null) {
      this.value.edit_due.payment_method = '';
    }
    if (this.value.edit_due.next_due !== null) {
      this.value.edit_due.next_due = this.value.edit_due.next_due.stamp_due_date;
    } else {
      this.value.edit_due.next_due = date;
    }
    if (this.value.edit_due.penalty_cost == null) {
      this.value.edit_due.penalty_cost = 0;
    }

  }

  ngOnInit() {
    this.url = "Upload a file";
    this.radioChange(this.value.edit_due.successful);
    this.createDueForm();
  }
  createDueForm() {
    this.dueForm = this.fb.group({
      successful: [this.value.edit_due.successful],
      verification_cert_no: [this.value.edit_due.verification_cert_no],
      verification_cert_url: [this.value.edit_due.verification_cert_url.url],
      status: [this.value.edit_due.status, Validators.required],
      verified_at: [this.value.edit_due.verified_at],
      stamping_cost: [this.value.edit_due.stamping_cost, Validators.required],
      next_due_date: [this.value.edit_due.next_due],
      payment_method: [this.value.edit_due.payment_method],
      site_cost: [this.value.edit_due.site_cost],
    });
  }

  radioChange(val) {
    let value = JSON.parse(val);
    if (value == true) {
      this.success = true;
    } else {
      this.success = false;
    }
  }
  statusChange(status) {
    if (status !== 'Closed') {
      this.dueForm
        .patchValue({
          successful: null
        });
      this.radioChange(null);
    }
  }

  onChangeImage(file) {
    console.log(file);
    this.image = file;
  }
  submit() {
    if (this.dueForm.value.status == 'Closed') {
      this.dueForm.controls['successful'].setValidators([Validators.required]);
      if (this.success == true) {
        this.dueForm.controls['verification_cert_no'].setValidators([Validators.required]);
        this.dueForm.controls['verified_at'].setValidators([Validators.required]);
        this.dueForm.controls['verification_cert_url'].setValidators([Validators.required]);
        this.dueForm.controls['next_due_date'].setValidators([Validators.required]);
        this.dueForm.controls['payment_method'].setValidators([Validators.required]);
        if (this.dueForm.value.verified_at == 'Site') {
          console.log('site')
          this.dueForm.controls['site_cost'].setValidators([Validators.required]);
        }
      }
    } else {
      this.dueForm.controls['successful'].clearValidators();
      this.dueForm.controls['verification_cert_no'].clearValidators();
      this.dueForm.controls['verified_at'].clearValidators();
      this.dueForm.controls['verification_cert_url'].clearValidators();
      this.dueForm.controls['next_due_date'].clearValidators();
      this.dueForm.controls['payment_method'].clearValidators();
      this.dueForm.controls['site_cost'].clearValidators();
    }
    this.dueForm.controls['verification_cert_no'].updateValueAndValidity();
    this.dueForm.controls['verified_at'].updateValueAndValidity();
    this.dueForm.controls['verification_cert_url'].updateValueAndValidity();
    this.dueForm.controls['next_due_date'].updateValueAndValidity();
    this.dueForm.controls['payment_method'].updateValueAndValidity();
    this.dueForm.controls['successful'].updateValueAndValidity();
    this.dueForm.controls['site_cost'].updateValueAndValidity();
    console.log(this.dueForm.value);
    Object.keys(this.dueForm.controls).forEach((key) => {
      console.log(typeof(this.dueForm.get(key).value))
      if(typeof(this.dueForm.get(key).value) == 'string' ) {
      this.dueForm.get(key).setValue(this.dueForm.get(key).value.trim())
      } 
    });
    if (this.dueForm.invalid) {
      return;
    }
    this.clicked = true;
    let data = this.dueForm.value;
    const stamp: FormData = new FormData();
    data.verification_cert_url = this.image;
    if (this.dueForm.value.status !== 'Closed') {
      stamp.append('stamping_due[status]', data.status);
      stamp.append('stamping_due[stamping_cost]', data.stamping_cost);
    } else {
      if (data.successful) {
        if (this.image) {
          stamp.append('stamping_due[verification_cert_url]', data.verification_cert_url);
        }
        stamp.append('stamping_due[successful]', data.successful);
        stamp.append('stamping_due[verification_cert_no]', data.verification_cert_no);
        stamp.append('stamping_due[status]', data.status);
        stamp.append('stamping_due[verified_at]', data.verified_at);
        stamp.append('stamping_due[stamping_cost]', data.stamping_cost);
        stamp.append('stamping_due[next_due_date]', data.next_due_date);
        stamp.append('stamping_due[payment_method]', data.payment_method);
        stamp.append('stamping_due[site_cost]', data.site_cost);
      } else {
        stamp.append('stamping_due[successful]', data.successful);
        stamp.append('stamping_due[status]', data.status);
        stamp.append('stamping_due[stamping_cost]', data.stamping_cost);  
      }

    }
console.log(data.successful)
    console.log(stamp);
    // stamp.forEach((value,key) => {
    //   console.log(key+value)
    //    });
    this.due.dues_put(stamp, this.value.due_id).pipe(untilDestroyed(this)).subscribe(res => {
      this.clicked = false;
      this.dialogRef.close();
      this.toast.success('Updated Successfully');
    }, error => {
      this.clicked = false;
      this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));      
    });
  }
  ngOnDestroy() { }

}
