import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { UpcomingDueComponent, upcoming_due_dialog_box } from './upcoming-due.component';
import { SharedModule} from 'app/shared';

const routes: Routes = [{ path: '', component: UpcomingDueComponent }];

@NgModule({
  declarations: [UpcomingDueComponent, upcoming_due_dialog_box],
  entryComponents: [upcoming_due_dialog_box],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
    exports: [RouterModule]
})
export class UpcomingDueModule { }
