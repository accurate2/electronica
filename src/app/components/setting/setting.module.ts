import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { SettingComponent } from './setting.component';
import { SharedModule } from 'app/shared';


const routes: Routes = [{ path: '', component: SettingComponent }];

@NgModule({
  declarations: [SettingComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
    exports: [RouterModule]
})
export class SettingModule { }