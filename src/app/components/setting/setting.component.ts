import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserService, SharedService } from 'services/app';
import { untilDestroyed } from 'ngx-take-until-destroy';


@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {

  settingForm: FormGroup;
  otpForm: FormGroup;
  userDetails: any;
  userId: any;
  validated = true;

  constructor(private user: UserService, private toast: ToastrService, private fb: FormBuilder, private sharedService: SharedService) { }

  ngOnInit() {
    this.sharedService.setLoaderShownProperty(false);
    this.createSettingForm();
    this.createOtpForm();
  }
  createSettingForm() {
    this.settingForm = this.fb.group({
      old_mob_number: ['', Validators.required],
      new_mob_number: ['', Validators.required]
    })
  }
  createOtpForm() {
    this.otpForm = this.fb.group({
      otp: ['', Validators.required],
    })
  }

  validate() {
    if (this.settingForm.controls.new_mob_number.invalid) {
      return;
    }
    console.log(this.settingForm.value.new_mob_number)
    this.user.validateUsername(this.settingForm.value.new_mob_number).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.sharedService.setLoaderShownProperty(false);
      if(res.status) {
         this.validated = false;
      } else {
        this.toast.error(res.message)
        this.validated = true;
      }
    })
  }
  getUser() {
    if (this.settingForm.invalid) {
      return;
    }
    this.user.getUser(this.settingForm.value).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      if (res.status) {
        this.userDetails = res.user;
        this.userId = res.user_id;
        this.toast.success(res.message);
        this.sharedService.setLoaderShownProperty(false);
        document.getElementById('color').setAttribute('readOnly', 'readOnly');
      } else {
        this.sharedService.setLoaderShownProperty(false);
        this.toast.error(res.message);
      }
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }

  verify() {
    if (this.otpForm.invalid) {
      return;
    }
    let data = {
      id: this.userId,
      verification_code: this.otpForm.value.otp,
      new_mob_no: this.settingForm.value.new_mob_number
    }
    console.log(data)
    this.user.otp_verification(data).pipe(untilDestroyed(this)).subscribe(res => {
      if (res.status) {
        this.toast.success(res.message);
        this.ngOnInit();
        this.userDetails = '';
        document.getElementById('color').removeAttribute('readOnly');
        window.scroll(0, 0);
      } else {
        this.toast.error(res.message);
      }
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }
  spaceNotallowed(event) {
    if (event.keyCode === 32) {
      return false;
    }
  }

  ngOnDestroy() { }

}
