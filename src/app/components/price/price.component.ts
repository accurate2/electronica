import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort } from '@angular/material';
export interface Service_Cost_Dialog_Data { }
export interface Stamping_Cost_Dialog_Data { }
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ToastrService } from 'ngx-toastr';
import { PriceService, ProductService, SharedService } from 'services/app';
import { TokenService } from 'services/core';


@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.scss'],
})
export class PriceComponent implements OnInit {
  pageNo: any;
  serviceTypes: any;
  service_scroll: any;
  stamping: any;
  stamping_scroll: any;
  searchValue = "";

  serviceCost_displayedColumns: string[] = ['position', 'type_name', 'cost', 'action'];
  serviceCost_dataSource = new MatTableDataSource();
  stampingCost_displayedColumns: string[] = ['position', 'capacity', 'accuracy', 'min_value', 'class_name', 'stamping_cost', 'action'];
  stampingCost_dataSource = new MatTableDataSource();

  @ViewChild('service_sort', { static: true }) service_sort: MatSort;
  @ViewChild('stamping_sort', { static: true }) stamping_sort: MatSort;

  sortingDataAccessor(item, property) {
    if (property.includes('.')) {
      return property.split('.')
        .reduce((object, key) => object[key], item);
    }
    return item[property];
  }


  constructor(private fb: FormBuilder, public dialog: MatDialog, private toast: ToastrService, private price: PriceService, private product: ProductService, public token: TokenService, private sharedService: SharedService) { }

  ngOnInit() {
    this.sharedService.setLoaderShownProperty(true);
    this.getMachineClass();
    if (this.token.getRole() === 'Verifier') {
      this.serviceCost_displayedColumns = ['position', 'type_name', 'cost'];
      this.stampingCost_displayedColumns = ['position', 'capacity', 'accuracy', 'min_value', 'class_name', 'stamping_cost'];
    }
  }

  select(tabGroup: { index: number }) {
    if (tabGroup.index === 0) {
      this.getMachineClass();
    } else {
      this.getServiceTypes();
    }
  }
  //Service Cost
  openServiceDialog(service, id): void {
    const dialogRef = this.dialog.open(service_cost_dialog_box, {
      width: 'auto',
      data: { edit_serviceCost: service, serviceCost_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getServiceTypes();
    });
  }

  getServiceTypes() {
    this.pageNo = 1;
    this.searchValue = "";
    this.price.serviceType_get(this.pageNo).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.serviceTypes = res;
      this.serviceCost_dataSource = new MatTableDataSource(this.serviceTypes);
      this.serviceCost_dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.serviceCost_dataSource.sort = this.service_sort;
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }
  applyFilter_service() {
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.price.serviceType_filter(this.pageNo, this.searchValue).pipe(untilDestroyed(this)).subscribe(res => {
        console.log(res);
        this.serviceTypes = res;
        this.serviceCost_dataSource = new MatTableDataSource(this.serviceTypes);
        this.serviceCost_dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.serviceCost_dataSource.sort = this.service_sort;
      });
    } else {
      this.getServiceTypes();
    }
  }
  onScroll_service() {
    this.pageNo = this.pageNo + 1;
    this.price.serviceType_filter(this.pageNo, this.searchValue).subscribe(res => {
      this.service_scroll = res;
      this.service_scroll.map(item => this.serviceTypes.push(item));
      this.serviceCost_dataSource = new MatTableDataSource(this.serviceTypes);
      this.serviceCost_dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.serviceCost_dataSource.sort = this.service_sort;
    })
  }

  // Stamping Cost

  openStampingDialog(stamping, id): void {
    const dialogRef = this.dialog.open(stamping_cost_dialog_box, {
      width: 'auto',
      data: { edit_stampingCost: stamping, stampingCost_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getMachineClass();
    });
  }

  getMachineClass() {
    this.pageNo = 1;
    this.searchValue = "";
    this.product.machine_class_get(this.pageNo).pipe(untilDestroyed(this)).subscribe(res => {
      this.stamping = res;
      this.stampingCost_dataSource = new MatTableDataSource(this.stamping);
      this.stampingCost_dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.stampingCost_dataSource.sort = this.stamping_sort;
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }
  applyFilter_stamping() {
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.product.machine_class_filter(this.pageNo, this.searchValue).pipe(untilDestroyed(this)).subscribe(res => {
        this.stamping = res;
        this.stampingCost_dataSource = new MatTableDataSource(this.stamping);
        this.stampingCost_dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.stampingCost_dataSource.sort = this.stamping_sort;
      });
    } else {
      this.getMachineClass();
    }
  }
  onScroll_stamping() {
    this.pageNo = this.pageNo + 1;
    this.product.machine_class_filter(this.pageNo, this.searchValue).subscribe(res => {
      this.stamping_scroll = res;
      this.stamping_scroll.map(item => this.stamping.push(item));
      this.stampingCost_dataSource = new MatTableDataSource(this.stamping);
      this.stampingCost_dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.stampingCost_dataSource.sort = this.stamping_sort;
    })
  }

  ngOnDestroy() { }

}

@Component({
  selector: 'service_cost_dialog_box',
  templateUrl: 'service_cost_dialog_box.html',
  styleUrls: ['./price.component.scss']
})

export class service_cost_dialog_box {

  serviceCostForm: FormGroup;
  value: any;
  clicked = false;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<service_cost_dialog_box>, @Inject(MAT_DIALOG_DATA) public data: Service_Cost_Dialog_Data,
    private toast: ToastrService, private price: PriceService) {
    this.value = this.data;
  }

  ngOnInit() {
    this.createServiceCostForm();
  }
  createServiceCostForm() {
    this.serviceCostForm = this.fb.group({
      type_name: [this.value.edit_serviceCost.type_name, Validators.required],
      cost: [this.value.edit_serviceCost.cost, Validators.required]
    });
  }
  save() {
    console.log(this.serviceCostForm.value);
    if (this.serviceCostForm.invalid) {
      this.toast.info('Please Enter Price Field');
      return;
    }
    this.clicked = true;
    let data = this.serviceCostForm.value;
    data.created_by_id = localStorage.getItem('user_id');
    this.price.serviceType_put(data, this.value.serviceCost_id).pipe(untilDestroyed(this)).subscribe(res => {
      this.clicked = false;
      this.dialogRef.close();
      this.toast.success('Price Updated Successfully');
    }, error => {
      this.clicked = false;      
      this.toast.error('Something Went Wrong');
    });
  }
  ngOnDestroy() { }
}

@Component({
  selector: 'stamping_cost_dialog_box',
  templateUrl: 'stamping_cost_dialog_box.html',
  styleUrls: ['./price.component.scss']
})

export class stamping_cost_dialog_box {

  stampingCostForm: FormGroup;
  value: any;
  clicked = false;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<service_cost_dialog_box>, @Inject(MAT_DIALOG_DATA) public data: Service_Cost_Dialog_Data,
    private toast: ToastrService, private product: ProductService) {
    this.value = this.data;
  }

  ngOnInit() {
    this.createStampingCostForm();
  }
  createStampingCostForm() {
    this.stampingCostForm = this.fb.group({
      class_name: [this.value.edit_stampingCost.class_name, Validators.required],
      stamping_cost: [this.value.edit_stampingCost.stamping_cost, Validators.required]
    });
  }
  save() {
    console.log(this.stampingCostForm.value);
    if (this.stampingCostForm.invalid) {
      this.toast.info('Please Enter Price Field');
      return;
    }
    this.clicked = true;
    let data = this.stampingCostForm.value;
    data.created_by_id = localStorage.getItem('user_id');
    this.product.machine_class_put(data, this.value.stampingCost_id).pipe(untilDestroyed(this)).subscribe(res => {
      this.clicked = false;
      this.dialogRef.close();
      this.toast.success('Price Updated Successfully');
    }, error => {
      this.clicked =false;
      this.toast.error('Something Went Wrong');
    });
  }
  ngOnDestroy() { }
}