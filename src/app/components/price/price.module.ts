import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { PriceComponent, service_cost_dialog_box, stamping_cost_dialog_box } from './price.component';
import { SharedModule } from 'app/shared';

const routes: Routes = [{ path: '', component: PriceComponent }];

@NgModule({
  declarations: [PriceComponent, service_cost_dialog_box, stamping_cost_dialog_box],
  entryComponents: [ service_cost_dialog_box, stamping_cost_dialog_box],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PriceModule { }
