import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort } from '@angular/material';
export interface MachineService_Dialog_Data { }
import { MachineServiceService, PriceService, SharedService, ReportService } from 'services/app';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { TokenService } from 'services/core';

@Component({
  selector: 'app-machine-service',
  templateUrl: './machine-service.component.html',
  styleUrls: ['./machine-service.component.scss']
})
export class MachineServiceComponent implements OnInit {

  machine_services: any;
  pageNo: number;
  machine_service_scroll: any;
  searchValue: "";
  dealers: any;
  dealer = "";
  customers: any;
  customer = "";
  status = "";
  panelOpenState = false;
  dealer_list: any;
  customer_list: any;
  dealer_input = "";
  customer_input = "";




  displayedColumns: string[] = ['position', 'user.full_name', 'customer.city.name', 'service_type.type_name', 'machine.mac_model', 'created_at', 'status', 'action'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  sortingDataAccessor(item, property) {
    if (property.includes('.')) {
      return property.split('.')
        .reduce((object, key) => object[key], item);
    }
    return item[property];
  }
  toggle() {
    this.panelOpenState = !this.panelOpenState
  }

  constructor(public dialog: MatDialog, private service: MachineServiceService, private toast: ToastrService, public token: TokenService, private sharedService: SharedService, private report: ReportService) { }

  ngOnInit() {
    this.sharedService.setLoaderShownProperty(true);
    this.getServices();
    if (this.token.getRole() === 'Verifier') {
      this.displayedColumns = ['position', 'user.full_name', 'customer.city.name', 'service_type.type_name', 'machine.mac_model', 'created_at', 'status'];
    }
    this.getDealers();
    this.getCustomers();
  }


  getServices() {
    this.pageNo = 1;
    this.searchValue = '';
    this.service.machine_service_get(this.pageNo, this.dealer, this.customer, this.status).pipe(untilDestroyed(this)).subscribe(res => {
      this.machine_services = res;
      this.dataSource = new MatTableDataSource(this.machine_services);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }

  applyFilter() {
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.service.machine_service_filter(this.pageNo, this.searchValue, this.dealer, this.customer, this.status).pipe(untilDestroyed(this)).subscribe(res => {
        this.machine_services = res;
        this.dataSource = new MatTableDataSource(this.machine_services);
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.dataSource.sort = this.sort;
      });
    } else {
      this.getServices();
    }
  }

  onScroll() {
    this.pageNo = this.pageNo + 1;
    this.service.machine_service_filter(this.pageNo, this.searchValue, this.dealer, this.customer, this.status).subscribe(res => {
      this.machine_service_scroll = res;
      this.machine_service_scroll.map(item => this.machine_services.push(item));
      this.dataSource = new MatTableDataSource(this.machine_services);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
    })
  }
  edit(user, id) {
    const dialogRef = this.dialog.open(machine_service_dialog_box, {
      width: 'auto',
      data: { edit_service: user, service_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getServices();
    });
  }

  delete(id) {
    Swal.fire({
      title: 'Are you sure want to delete?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.service.machine_service_delete(id).pipe(untilDestroyed(this)).subscribe(res => {
          this.toast.success('Deleted Successfully');
          this.ngOnInit();
        }, error => {
          this.toast.error('Something Went Wrong');
        });
      }
    });
  }
  getDealers() {
    this.report.dealers_get().pipe(untilDestroyed(this)).subscribe(res => {
      this.dealers = res;
      this.dealer_list = res;
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_dealer(event) {
    let value = event.trim().toLowerCase();
    this.dealers = this.dealer_list.filter(dealer => dealer.full_name.toLowerCase().indexOf(value) > -1);
    if (this.dealers.length === 0) {
      this.dealer = ""
    }
  }
  getCustomers(event?) {
    this.customer = "";
    this.customer_input = "";
    this.report.customers_get(event).pipe(untilDestroyed(this)).subscribe(res => {
      this.customers = res;
      this.customer_list = res;
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_customer(event) {
    let value = event.trim().toLowerCase();
    this.customers = this.customer_list.filter(customer => customer.full_name.toLowerCase().indexOf(value) > -1);
    if (this.customers.length === 0) {
      this.customer = "";
    }
  }
  filter() {
    console.log(this.dealer, this.customer);
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.service.machine_service_filter(this.pageNo, this.searchValue, this.dealer, this.customer, this.status).pipe(untilDestroyed(this)).subscribe(res => {
        this.machine_services = res;
        this.dataSource = new MatTableDataSource(this.machine_services);
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.dataSource.sort = this.sort;
        this.sharedService.setLoaderShownProperty(false);
      }, error => {
        this.toast.error('Something Went Wrong');
      });
    } else {
      this.getServices();
    }
  }
  clear() {
    this.dealer = "";
    this.customer = "";
    this.status = "";
    this.dealer_input = "";
    this.customer_input = "";
    this.ngOnInit();
  }

  ngOnDestroy() { }

}
@Component({
  selector: 'machine_service_dialog_box',
  templateUrl: 'machine_service_dialog_box.html',
  styleUrls: ['./machine-service.component.scss']
})

export class machine_service_dialog_box {

  value: any;
  searchValue: any;
  total_amount: any;
  extra_amount = 0;
  clicked = false;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<machine_service_dialog_box>, @Inject(MAT_DIALOG_DATA) public data: MachineService_Dialog_Data,
    private toast: ToastrService, private service: MachineServiceService, private price: PriceService, public token: TokenService) {
    this.value = this.data;
  }

  serviceForm: FormGroup;
  pageNo: any;

  ngOnInit() {
    this.createServiceForm();
  }
  createServiceForm() {
    this.total_amount = this.value.edit_service.service_cost;
    if (this.value.edit_service.total_cost == null) {
      this.value.edit_service.total_cost = this.total_amount;
    }
    console.log(this.value.edit_service.total_cost)
    this.serviceForm = this.fb.group({
      service_cost: [this.value.edit_service.service_cost, Validators.required],
      description: [this.value.edit_service.description],
      status: [this.value.edit_service.status, Validators.required],
      extra_charges: [this.value.edit_service.extra_charges],
      discount_percentage: [this.value.edit_service.discount_percentage],
      discount_amount: [this.value.edit_service.discount_amount],
      total_cost: [this.value.edit_service.total_cost]
    })
  }

  submit() {
    Object.keys(this.serviceForm.controls).forEach((key) => {
      console.log(typeof(this.serviceForm.get(key).value))
      if(typeof(this.serviceForm.get(key).value) == 'string' ) {
      this.serviceForm.get(key).setValue(this.serviceForm.get(key).value.trim())
      } 
    });
    console.log(this.serviceForm.value);
    if (this.serviceForm.invalid) {
      // this.toast.info('Please Enter All Fields');
      return;
    }
    this.clicked = true;
    this.service.machine_service_put(this.serviceForm.value, this.value.service_id).pipe(untilDestroyed(this)).subscribe(res => {
      this.clicked = false;
      this.dialogRef.close();
      this.toast.success('Updated Successfully');
    }, error => {
      this.clicked = false;
      this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
    });
  }

  discount(event) {
    if (event.target.value > 100) {
      event.preventDefault();
      this.serviceForm.controls['discount_percentage'].setErrors({ lessthan: true });
      return false;
    }
    if ((event.target.value <= 100) && (0 <= event.target.value)) {
      let discount = event.target.value;
      discount = (discount / 100) * this.value.edit_service.service_cost;
      this.serviceForm.patchValue({
        discount_amount: discount
      });
      let total = (this.serviceForm.value.service_cost - discount) + this.extra_amount;
      let total1 = this.serviceForm.value.service_cost - discount;
      this.serviceForm.patchValue({
        total_cost: total
      });
      this.total_amount = total1;
    }

  }
  extraCharges(event) {
    console.log(this.serviceForm.value.extra_charges)
    let extra = event.target.value;
    extra = extra > 1 ? extra : 0;
    this.extra_amount = parseInt(extra);
    extra = this.total_amount + parseInt(extra);
    this.serviceForm.patchValue({
      total_cost: extra
    });
  }

  ngOnDestroy() { }
}
