import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MachineServiceComponent, machine_service_dialog_box } from './machine-service.component';
import { SharedModule } from 'app/shared';

const routes: Routes = [{ path: '', component: MachineServiceComponent }];

@NgModule({
  declarations: [MachineServiceComponent, machine_service_dialog_box],
  entryComponents: [machine_service_dialog_box],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
    exports: [RouterModule]
})
export class MachineServiceModule { }
