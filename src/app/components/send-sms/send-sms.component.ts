import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort } from '@angular/material';
import { DealerService, CustomerService, SharedService, UserService, ReportService } from 'services/app';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ToastrService } from 'ngx-toastr';
import { TokenService } from 'app/services';
import { SelectionModel } from '@angular/cdk/collections';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
export interface SendSMS_Dialog_Data { }



@Component({
  selector: 'app-send-sms',
  templateUrl: './send-sms.component.html',
  styleUrls: ['./send-sms.component.scss']
})
export class SendSmsComponent implements OnInit {

  dealers: any;
  pageNo: number;
  dealer_scroll: any;
  dealerID: any;
  searchValue = "";
  customers: any;
  customer_scroll: any;
  customerID: any[] = [];
  customerCount: any;
  dealerCount: any;
  dealerFilter = "";
  sms_limit: any;
  sms_used: any;
  sms_available: any;
  state = "";
  city = "";
  area = "";
  state1 = "";
  city1 = "";
  area1 = "";
  si_circle = "";
  panelOpenState = false;
  panelOpenState1 = false;
  states: any;
  cities: any;
  areas: any;
  stateId: any;
  cityId: any;
  states1: any;
  cities1: any;
  areas1: any;
  stateId1: any;
  cityId1: any;
  dealer_list: any;
  state_list: any;
  city_list: any;
  area_list: any;
  dealer_list1: any;
  state_list1: any;
  city_list1: any;
  area_list1: any;
  state_input = "";
  city_input = "";
  state_input1 = "";
  city_input1 = "";
  area_input = "";
  area_input1= "";
  dealer_input = "";



  displayedColumns_dealer: string[] = ['select', 'position', 'user.uuid', 'user.full_name', 'user.username', 'address_line1', 'city.name', 'user.status'];
  dataSource_dealer = new MatTableDataSource();

  displayedColumns_customer: string[] = ['select', 'position', 'user.uuid', 'user.full_name', 'user.username', 'address_line1', 'city.name', 'qrcode'];
  dataSource_customer = new MatTableDataSource();
  selection = new SelectionModel(true, []);
  selection1 = new SelectionModel(true, []);

  @ViewChild('dealer_sort', { static: true }) dealer_sort: MatSort;
  @ViewChild('customer_sort', { static: true }) customer_sort: MatSort;


  sortingDataAccessor(item, property) {
    if (property.includes('.')) {
      return property.split('.')
        .reduce((object, key) => object[key], item);
    }
    return item[property];
  }
  //dealer checkbox
  isAllSelected_dealer() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource_dealer.data.length;
    return numSelected === numRows;
  }

  masterToggle_dealer() {
    this.isAllSelected_dealer() ?
      this.selection.clear() :
      this.dataSource_dealer.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel_dealer(row?): string {
    if (!row) {
      return `${this.isAllSelected_dealer() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  //customer checkbox 
  isAllSelected_customer() {
    const numSelected = this.selection1.selected.length;
    const numRows = this.dataSource_customer.data.length;
    return numSelected === numRows;
  }

  masterToggle_customer() {
    this.isAllSelected_customer() ?
      this.selection1.clear() :
      this.dataSource_customer.data.forEach(row => this.selection1.select(row));
  }

  checkboxLabel_customer(row?): string {
    if (!row) {
      return `${this.isAllSelected_customer() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection1.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  toggle() {
    this.panelOpenState = !this.panelOpenState
  }

  toggle1() {
    this.panelOpenState1 = !this.panelOpenState1
  }

  constructor(public dialog: MatDialog, private dealer: DealerService, private toast: ToastrService, public token: TokenService, private customer: CustomerService, private sharedService: SharedService, private user: UserService, private report: ReportService) { }

  ngOnInit() {
    this.getDealers();
    this.getCustomers();
    this.selection.clear();
    this.selection1.clear();
    this.getCount();
    this.getSms_credit();
    this.getStates();
    this.getCities();
    this.getAreas();
    this.getStates1();
    this.getCities1();
    this.getAreas1();
    this.getDealerList();
    //   if (this.token.getRole() === 'Verifier') {
    //   this.displayedColumns_dealer = ['position', 'user.uuid', 'user.full_name', 'user.username', 'address_line1', 'city', 'user.status'];
    //   this.displayedColumns_customer = ['select', 'position', 'user.uuid', 'user.full_name', 'user.username', 'address_line1', 'city', 'qrcode'];
    // }
  }
  select(tabGroup: { index: number }) {
    this.customerID = [];
    if (tabGroup.index === 0) {
      this.getDealers();
      this.selection.clear();
    } else {
      this.getCustomers();
      this.selection1.clear();
    }
  }

  getCount() {
    this.user.getDealerCusCount().pipe(untilDestroyed(this)).subscribe(res => {
      this.dealerCount = res.dealer;
      this.customerCount = res.customer;
    });
  }

  getSms_credit() {
    this.user.getSms_credit().pipe(untilDestroyed(this)).subscribe(res => {
      let data = res.data;
      this.sms_limit = data.limit.replace(/'/g, "");
      this.sms_used = data.used.replace(/'/g, "");
      this.sms_available = parseInt(this.sms_limit) - parseInt(this.sms_used) ; 
  });
}


//Dealers

getStates() {
  this.report.state_get().pipe(untilDestroyed(this)).subscribe(res => {
    console.log(res);
    this.states = res;
    this.state_list = res;
  });
}
search_state(event) {
  let value = event.trim().toLowerCase();
  this.states = this.state_list.filter(state => state.name.toLowerCase().indexOf(value) > -1);
  if (this.states.length === 0) {
    this.state = "";
  }
}
getCities(event?) {
  this.city = "";
  this.area = "";
  this.city_input = "";
  this.area_input = "";
  this.stateId = event;
  this.report.city_get(this.stateId).pipe(untilDestroyed(this)).subscribe(res => {
    console.log(res);
    this.cities = res;
    this.city_list = res;
  });
}
search_city(event) {
  let value = event.trim().toLowerCase();
  this.cities = this.city_list.filter(city => city.name.toLowerCase().indexOf(value) > -1);
  if (this.cities.length === 0) {
    this.city = "";
  }
}
getAreas(event?) {
  this.area = "";
  this.area_input = "";
  this.cityId = event;
  this.report.area_get(this.stateId, this.cityId).pipe(untilDestroyed(this)).subscribe(res => {
    console.log(res);
    this.areas = res;
    this.area_list = res;
  });
}
search_area(event) {
  let value = event.trim().toLowerCase();
  this.areas = this.area_list.filter(area => area.name.toLowerCase().indexOf(value) > -1);
  if (this.areas.length === 0) {
    this.area = "";
  }
}

getDealers() {
  this.pageNo = 1;
  this.searchValue = "";
  this.dealer.dealer_get(this.pageNo, this.state, this.city, this.area).pipe(untilDestroyed(this)).subscribe(res => {
    this.dealers = res;
    this.dataSource_dealer = new MatTableDataSource(this.dealers);
    this.dataSource_dealer.sortingDataAccessor = this.sortingDataAccessor;
    this.dataSource_dealer.sort = this.dealer_sort;
    this.sharedService.setLoaderShownProperty(false);

  }, error => {
    this.toast.error('Something Went Wrong');
  });
}
applyFilter_dealer() {
  this.pageNo = 1;
  if (this.searchValue.length > 0) {
    this.dealer.dealer_filter(this.pageNo, this.searchValue, this.state, this.city, this.area).pipe(untilDestroyed(this)).subscribe(res => {
      this.dealers = res;
      this.dataSource_dealer = new MatTableDataSource(this.dealers);
      this.dataSource_dealer.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource_dealer.sort = this.dealer_sort;
    });
  } else {
    this.getDealers();
  }
}
onScroll_dealer() {
  this.pageNo = this.pageNo + 1;
  // this.searchValue = '';
  console.log(this.pageNo)
  this.dealer.dealer_filter(this.pageNo, this.searchValue, this.state, this.city, this.area).subscribe(res => {
    this.dealer_scroll = res;
    this.dealer_scroll.map(item => this.dealers.push(item));
    this.dataSource_dealer = new MatTableDataSource(this.dealers);
    this.dataSource_dealer.sortingDataAccessor = this.sortingDataAccessor;
    this.dataSource_dealer.sort = this.dealer_sort;
  })
}
selectAll_dealer(event) {
  if (event.checked) {
    this.customerID = [];
  }
  this.dealers.map(item => {
    if(item.user.status !== 'Inactive') {
      this.selectDealer(item.user.id, event)
    }
  });
}
selectDealer(cus_id, event) {
  if (event.checked) {
    this.customerID.push(cus_id);
  } else {
    const index: number = this.customerID.indexOf(cus_id);
    if (index > -1) {
      this.customerID.splice(index, 1);
    }
  }
  console.log(this.customerID);
}

filter_dealer() {
  this.dealer.dealer_filter(this.pageNo, this.searchValue, this.state, this.city, this.area).pipe(untilDestroyed(this)).subscribe(res => {
    this.dealers = res;
    this.dataSource_dealer = new MatTableDataSource(this.dealers);
    this.dataSource_dealer.sortingDataAccessor = this.sortingDataAccessor;
    this.dataSource_dealer.sort = this.dealer_sort;
  });
}
clear_dealer() {
  this.state = "";
  this.city = "";
  this.area = "";
  this.state_input = "";
  this.city_input = "";
  this.area_input = "";
  this.ngOnInit();
}

//customers
getDealerList() {
  this.report.dealers_get().pipe(untilDestroyed(this)).subscribe(res => {
    console.log(res)
    this.dealer_list = res;
    this.dealer_list1 = res;
  }, error => {
    // this.toast.error('Something Went Wrong');
  });
}
search_dealer(event) {
  let value = event.trim().toLowerCase();
  this.dealer_list = this.dealer_list1.filter(dealer => dealer.full_name.toLowerCase().indexOf(value) > -1);
  if (this.dealer_list.length === 0) {
    this.dealerFilter = "";
  }
}
getStates1() {
  this.report.state_get().pipe(untilDestroyed(this)).subscribe(res => {
    console.log(res);
    this.states1 = res;
    this.state_list1 = res;
  });
}
search_state1(event) {
  let value = event.trim().toLowerCase();
  this.states1 = this.state_list1.filter(state => state.name.toLowerCase().indexOf(value) > -1);
  if (this.states1.length === 0) {
    this.state1 = "";
  }
}

getCities1(event?) {
  this.city1 = "";
  this.area1 = "";
  this.city_input1 = "";
  this.area_input1 = "";
  this.stateId1 = event;
  this.report.city_get(this.stateId1).pipe(untilDestroyed(this)).subscribe(res => {
    this.cities1 = res;
    this.city_list1 = res;
  });
}
search_city1(event) {
  let value = event.trim().toLowerCase();
  this.cities1 = this.city_list1.filter(city => city.name.toLowerCase().indexOf(value) > -1);
  if (this.cities1.length === 0) {
    this.city1 = "";
  }
}

getAreas1(event?) {
  this.area_input1 = "";
  this.cityId1 = event;
  this.report.area_get(this.stateId1, this.cityId1).pipe(untilDestroyed(this)).subscribe(res => {
    console.log(res);
    this.areas1 = res;
    this.area_list1 = res;
  });
}

search_area1(event) {
  let value = event.trim().toLowerCase();
  this.areas1 = this.area_list1.filter(area => area.name.toLowerCase().indexOf(value) > -1);
  if (this.areas1.length === 0) {
    this.area1 = "";
  }
}

getCustomers() {
  this.pageNo = 1;
  this.searchValue = "";
  this.customer.customer_get(this.pageNo, this.dealerFilter, this.state1, this.city1, this.area1, this.si_circle).pipe(untilDestroyed(this)).subscribe(res => {
    this.customers = res;
    this.dataSource_customer = new MatTableDataSource(this.customers);
    this.dataSource_customer.sortingDataAccessor = this.sortingDataAccessor;
    this.dataSource_customer.sort = this.customer_sort;
  }, error => {
    this.toast.error('Something Went Wrong');
  });
}
applyFilter_customer() {
  this.pageNo = 1;
  if (this.searchValue.length > 0) {
    this.customer.customer_filter(this.pageNo, this.searchValue, this.dealerFilter, this.state1, this.city1, this.area1, this.si_circle).pipe(untilDestroyed(this)).subscribe(res => {
      this.customers = res;
      this.dataSource_customer = new MatTableDataSource(this.customers);
      this.dataSource_customer.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource_customer.sort = this.customer_sort;
    });
  } else {
    this.getCustomers();
  }
}
onScroll_customer() {
  this.pageNo = this.pageNo + 1;
  this.customer.customer_filter(this.pageNo, this.searchValue, this.dealerFilter, this.state1, this.city1, this.area1,this.si_circle).subscribe(res => {
    this.customer_scroll = res;
    this.customer_scroll.map(item => this.customers.push(item));
    this.dataSource_customer = new MatTableDataSource(this.customers);
    this.dataSource_customer.sortingDataAccessor = this.sortingDataAccessor;
    this.dataSource_customer.sort = this.customer_sort;
  })
}

filter_customer() {
  this.customer.customer_filter(this.pageNo, this.searchValue, this.dealerFilter, this.state1, this.city1, this.area1,this.si_circle).pipe(untilDestroyed(this)).subscribe(res => {
    this.customers = res;
    this.dataSource_customer = new MatTableDataSource(this.customers);
    this.dataSource_customer.sortingDataAccessor = this.sortingDataAccessor;
    this.dataSource_customer.sort = this.customer_sort;
  });
}

clear_customer() {
  this.state1 = "";
  this.city1 = "";
  this.area1 = "";
  this.dealerFilter = "";
  this.state_input1 = "";
  this.city_input1 = "";
  this.area_input1= "";
  this.dealer_input = "";
  this.ngOnInit();
}


selectAll_customer(event) {
  if (event.checked) {
    this.customerID = [];
  }
  this.customers.map(item => this.selectCustomer(item.user.id, event));
}
selectCustomer(cus_id, event) {
  if (event.checked) {
    this.customerID.push(cus_id);
  } else {
    const index: number = this.customerID.indexOf(cus_id);
    console.log(index);
    if (index > -1) {
      this.customerID.splice(index, 1);
    }
  }
  console.log(this.customerID);
}
openDialog(): void {
  console.log(this.customerID.length);
  if(this.customerID.length === 0) {
  this.toast.info('Please select atleast one dealer/customer');
} else {
  const dialogRef = this.dialog.open(send_sms_dialog_box, {
    width: '50%',
    data: { id: this.customerID }
  });
  dialogRef.afterClosed().subscribe(() => {
    this.ngOnInit();
    this.customerID = [];
  });
}
  }


ngOnDestroy() { }
}

@Component({
  selector: 'send_sms_dialog_box',
  templateUrl: 'send_sms_dialog_box.html',
})

export class send_sms_dialog_box {

  smsForm: FormGroup;
  value: any;
  clicked = false;


  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<send_sms_dialog_box>, @Inject(MAT_DIALOG_DATA) public data: SendSMS_Dialog_Data,
    private toast: ToastrService, private user: UserService) {
    this.value = this.data;
  }

  ngOnInit() {
    this.createSMSForm();
  }
  createSMSForm() {
    this.smsForm = this.fb.group({
      message: ['', Validators.required]
    });
  }

  submit() {
    console.log(this.smsForm.value);
    if (this.smsForm.invalid) {
      this.toast.info('Please Enter a message');
      return;
    }
    this.clicked = true;
    let data = {
      "ids": this.value.id,
      "message": this.smsForm.value.message
    }
    console.log(data);
    this.user.send_sms(data).pipe(untilDestroyed(this)).subscribe(res => {
      this.clicked = false;
      if (res.status) {
        this.dialogRef.close();
        this.ngOnInit();
        this.toast.success(res.message);
      } else {
        this.toast.error(res.message);
      }
    }, error => {
      this.clicked = false;
      this.toast.error('Something Went Wrong');
    });
  }

  ngOnDestroy() { }

}