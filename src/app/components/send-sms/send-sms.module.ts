import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { SendSmsComponent, send_sms_dialog_box } from './send-sms.component';
import { SharedModule } from 'app/shared';


const routes: Routes = [{ path: '', component: SendSmsComponent }];

@NgModule({
  declarations: [SendSmsComponent, send_sms_dialog_box],
  entryComponents: [ send_sms_dialog_box ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]

})
export class SendSmsModule { }

