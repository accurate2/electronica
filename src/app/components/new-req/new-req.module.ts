import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { NewReqComponent, new_req_dialog_box } from './new-req.component';
import { SharedModule } from 'app/shared';

const routes: Routes = [{ path: '', component: NewReqComponent }];

@NgModule({
  declarations: [NewReqComponent, new_req_dialog_box],
  entryComponents: [new_req_dialog_box],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
    exports: [RouterModule]
})
export class NewReqModule { }
