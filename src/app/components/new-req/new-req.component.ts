import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort } from '@angular/material';
export interface New_req_Dialog_Data { }
import { NewReqService, ProductService, SharedService, ReportService } from 'services/app';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { TokenService } from 'services/core';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-new-req',
  templateUrl: './new-req.component.html',
  styleUrls: ['./new-req.component.scss']
})
export class NewReqComponent implements OnInit {
  pageNo: any;
  requirementList: any;
  requirement_scroll: any;
  searchValue = "";
  customers: any;
  customer_list: any;
  customer = "";
  status = "";
  purchase_date = "";
  purchase_date_begin = "";
  purchase_date_end = "";
  panelOpenState = false;
  customer_input = "";


  displayedColumns: string[] = ['position', 'user.full_name', 'user.username', 'customer.city.name', 'created_at', 'expected_purchase_date', 'status', 'action'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  sortingDataAccessor(item, property) {
    if (property.includes('.')) {
      return property.split('.')
        .reduce((object, key) => object[key], item);
    }
    return item[property];
  }
  toggle() {
    this.panelOpenState = !this.panelOpenState
  }

  constructor(public dialog: MatDialog, private new_req: NewReqService, private toast: ToastrService, public token: TokenService, private sharedService: SharedService, private report: ReportService, public datepipe: DatePipe) { }

  ngOnInit() {
    this.sharedService.setLoaderShownProperty(true);
    this.getRequirements();
    if (this.token.getRole() === 'Verifier') {
      this.displayedColumns = ['position', 'user.full_name', 'user.username', 'customer.city.name', 'created_at', 'status'];
    }
    this.getCustomers();
  }

  getRequirements() {
    this.pageNo = 1;
    this.searchValue = "";
    this.new_req.new_req_get(this.pageNo, this.customer, this.status, this.purchase_date_begin, this.purchase_date_end).pipe(untilDestroyed(this)).subscribe(res => {
      this.requirementList = res;
      this.dataSource = new MatTableDataSource(this.requirementList);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }

  applyFilter() {
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.new_req.new_req_filter(this.pageNo, this.searchValue, this.customer, this.status, this.purchase_date_begin, this.purchase_date_end).pipe(untilDestroyed(this)).subscribe(res => {
        this.requirementList = res;
        this.dataSource = new MatTableDataSource(this.requirementList);
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.dataSource.sort = this.sort;
      });
    } else {
      this.getRequirements();
    }
  }

  onScroll() {
    this.pageNo = this.pageNo + 1;
    this.new_req.new_req_filter(this.pageNo, this.searchValue, this.customer, this.status, this.purchase_date_begin, this.purchase_date_end).subscribe(res => {
      this.requirement_scroll = res;
      this.requirement_scroll.map(item => this.requirementList.push(item));
      this.dataSource = new MatTableDataSource(this.requirementList);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
    })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(new_req_dialog_box, {
      width: 'auto',
      data: { new: 'new' }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getRequirements();
    });
  }

  edit(user, id) {
    const dialogRef = this.dialog.open(new_req_dialog_box, {
      width: 'auto',
      data: { edit_service: user, service_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getRequirements();
    });
  }

  delete(id) {
    Swal.fire({
      title: 'Are you sure want to delete?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.new_req.new_req_delete(id).pipe(untilDestroyed(this)).subscribe(res => {
          this.toast.success('Deleted Successfully');
          this.ngOnInit();
        }, error => {
          this.toast.error('Something Went Wrong');
        });
      }
    });
  }
  getCustomers(event?) {
    this.report.customers_get(event).pipe(untilDestroyed(this)).subscribe(res => {
      this.customers = res;
      this.customer_list = res;
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_customer(event) {
    let value = event.trim().toLowerCase();
    this.customers = this.customer_list.filter(customer => customer.full_name.toLowerCase().indexOf(value) > -1);
    if (this.customers.length === 0) {
      this.customer = "";
    }
  }
  purchaseDate(event) {
    console.log(event);
    this.purchase_date_begin = this.datepipe.transform(event.begin, 'yyyy-MM-dd');
    this.purchase_date_end = this.datepipe.transform(event.end, 'yyyy-MM-dd');
  }
  filter() {
    console.log(this.purchase_date);
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.new_req.new_req_filter(this.pageNo, this.searchValue, this.customer, this.status, this.purchase_date_begin, this.purchase_date_end).pipe(untilDestroyed(this)).subscribe(res => {
        this.requirementList = res;
        this.dataSource = new MatTableDataSource(this.requirementList);
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.dataSource.sort = this.sort;
        this.sharedService.setLoaderShownProperty(false);
      }, error => {
        this.toast.error('Something Went Wrong');
      });
    } else {
      this.getRequirements();
    }
  }
  clear() {
    this.customer = "";
    this.status = "";
    this.purchase_date = "";
    this.purchase_date_begin = "";
    this.purchase_date_end = "";
    this.customer_input = "";
    this.ngOnInit();
  }

  ngOnDestroy() { }

}

//New requirement edit 

@Component({
  selector: 'new_req_dialog_box',
  templateUrl: 'new_req_dialog_box.html',
})

export class new_req_dialog_box {

  reqForm: FormGroup;
  value: any;
  pageNo: any;
  capacities: any;
  accuracies: any;
  machine_class: any;
  searchValue: any;
  capacity: any;
  accuracy: any;
  capacityId: any;
  accuracyId: any;
  customers: any;
  customer_list: any;
  clicked = false;
  minDate = new Date();





  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<new_req_dialog_box>, @Inject(MAT_DIALOG_DATA) public data: New_req_Dialog_Data,
    private new_req: NewReqService, private toast: ToastrService, private product: ProductService, public token: TokenService, private report: ReportService) {
    this.value = this.data;
    console.log(this.value)
  }

  ngOnInit() {
    this.getCustomers();
    this.getCapacities();
    if (!this.value.new) {
      this.capacityId = this.value.edit_service.machine_class.capacity;
      this.accuracyId = this.value.edit_service.machine_class.accuracy;
    }
    this.createNewReqForm();
  }
  createNewReqForm() {
    if (this.value.new) {
      this.reqForm = this.fb.group({
        status: ['Open'],
        make: ['', Validators.required],
        capacity_id: ['', Validators.required],
        accuracy_id: ['', Validators.required],
        machine_class_id: ['', Validators.required],
        requirements: [''],
        expected_purchase_date: ['', Validators.required],
        customer_id: ['', Validators.required]
      })
    } else {
      this.reqForm = this.fb.group({
        status: [this.value.edit_service.status, Validators.required],
        make: [this.value.edit_service.make, Validators.required],
        capacity_id: [this.value.edit_service.machine_class.capacity, Validators.required],
        accuracy_id: [this.value.edit_service.machine_class.accuracy, Validators.required],
        machine_class_id: [this.value.edit_service.machine_class.id, Validators.required],
        requirements: [this.value.edit_service.requirements],
        expected_purchase_date: [this.value.edit_service.expected_purchase_date, Validators.required]
      })
    }
  }
  getCapacities() {
    this.pageNo = 1;
    this.product.capacity_get(this.pageNo).pipe(untilDestroyed(this)).subscribe(res => {
      this.capacities = res.data;
      this.capacity = res.data;
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_Capacity(event) {
    let value = event.trim().toLowerCase();
    this.capacities = this.capacity.filter(capacity => capacity.toLowerCase().indexOf(value) > -1);
  }

  getAccuraciesByCapacity(capacity) {
    this.capacityId = capacity;
    this.product.accuracies_getByCapacity(capacity).pipe(untilDestroyed(this)).subscribe(res => {
      this.accuracies = res.data;
      this.accuracy = res.data;
      this.onChangeAccuracy(this.accuracies[0]);
      this.reqForm
        .patchValue({
          accuracy_id: this.accuracies[0]
        });
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_Accuracy(event) {
    let value = event.trim().toLowerCase();
    this.accuracies = this.accuracy.filter(accuracy => accuracy.toLowerCase().indexOf(value) > -1);
  }

  onChangeAccuracy(val) {
    this.accuracyId = val;
    this.getMachineBycapacity(this.capacityId, this.accuracyId);
  }

  getMachineBycapacity(capacity, accuracy) {
    this.product.machine_class_getByCapacity(capacity, accuracy).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.machine_class = res;
      this.reqForm
        .patchValue({
          machine_class_id: this.machine_class.class_name
        });
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  getCustomers(event?) {
    this.report.customers_get(event).pipe(untilDestroyed(this)).subscribe(res => {
      this.customers = res;
      this.customer_list = res;
      console.log(this.customers);

    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_customer(event) {
    let value = event.trim().toLowerCase();
    this.customers = this.customer_list.filter(customer => customer.full_name.toLowerCase().indexOf(value) > -1);
    if (this.customers.length === 0) {
      this.reqForm.patchValue({
        customer_id: ""
      })
    }
  }

  submit() {
    Object.keys(this.reqForm.controls).forEach((key) => {
      console.log(typeof(this.reqForm.get(key).value))
      if(typeof(this.reqForm.get(key).value) == 'string' ) {
      this.reqForm.get(key).setValue(this.reqForm.get(key).value.trim())
      } 
    });
    if (this.reqForm.invalid) {
      return;
    }
    this.clicked = true;
    let data = this.reqForm.value;
    data.make = data.make.toUpperCase();
    if (this.value.new) {
      data.machine_class_id = this.machine_class.id;
      console.log(data);
      this.new_req.new_req_create(data).pipe(untilDestroyed(this)).subscribe(res => {
        this.clicked = false;
        this.dialogRef.close();
        this.toast.success('Created Successfully');
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    } else {
      this.new_req.new_req_put(data, this.value.service_id).pipe(untilDestroyed(this)).subscribe(res => {
        this.clicked = false;       
        this.dialogRef.close();
        this.toast.success('Updated Successfully');
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    }
  }
  ngOnDestroy() { }
}