import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { BannerComponent, banner_dialog_box } from './banner.component';
import { SharedModule } from 'app/shared';

const routes: Routes = [{ path: '', component: BannerComponent }];

@NgModule({
  declarations: [BannerComponent, banner_dialog_box],
  entryComponents: [ banner_dialog_box],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class BannerModule { }
