import { Component, OnInit, Inject, ViewChild, HostListener } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
export interface Banner_Dialog_Data { }
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ToastrService } from 'ngx-toastr';
import { BannerService, SharedService } from 'services/app';
import Swal from 'sweetalert2';
import { TokenService } from 'services/core';
import { environment } from 'environments/environment';





@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

  banners: any;
  pageNo: number;
  banners_scroll: any;
  displayedColumns: string[] = ['position', 'banner_url.url', 'description', 'action'];
  dataSource = new MatTableDataSource();
  BaseURL: string = environment.imageUrl;
  
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  sortingDataAccessor(item, property) {
    if (property.includes('.')) {
      return property.split('.')
        .reduce((object, key) => object[key], item);
    }
    return item[property];
  }


  constructor(public dialog: MatDialog, private toast: ToastrService, private banner: BannerService, public token: TokenService, private sharedService: SharedService) { }

  ngOnInit() {
    this.sharedService.setLoaderShownProperty(true);
    this.getBanners();
    if (this.token.getRole() === 'Verifier') {
      this.displayedColumns = ['position', 'banner_url.url', 'description'];
    }
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(banner_dialog_box, {
      width: 'auto',
      data: { new: 'new' }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getBanners();
    });
  }

  edit(banner, id) {
    const dialogRef = this.dialog.open(banner_dialog_box, {
      width: 'auto',
      data: { edit_banner: banner, banner_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getBanners();
    });
  }

  delete(id) {
    Swal.fire({
      title: 'Are you sure want to delete?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.banner.banner_delete(id).pipe(untilDestroyed(this)).subscribe(res => {
          this.toast.success('Deleted Successfully');
          this.ngOnInit();
        }, error => {
          this.toast.error('Something Went Wrong');
        });
      }
    });
  }

  getBanners() {
    this.pageNo = 1;
    this.banner.banner_get(this.pageNo).pipe(untilDestroyed(this)).subscribe(res => {
      this.banners = res;
      this.dataSource = new MatTableDataSource(this.banners);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      this.toast.error('SomethingWent Wrong');
    });
  }
  
  onScroll() {
    this.pageNo = this.pageNo + 1;
    console.log(this.pageNo)
    this.banner.banner_get(this.pageNo).subscribe(res => {
      this.banners_scroll = res;
      this.banners_scroll.map(item => this.banners.push(item));
      this.dataSource = new MatTableDataSource(this.banners);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
    })
  }

  ngOnDestroy() { }
}
@Component({
  selector: 'banner_dialog_box',
  templateUrl: 'banner_dialog_box.html',
})

export class banner_dialog_box {

  bannerForm: FormGroup;
  image: any;
  value: any;
  url: any;
  clicked = false;


  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<banner_dialog_box>, @Inject(MAT_DIALOG_DATA) public data: Banner_Dialog_Data,
    private toast: ToastrService, private banner: BannerService) {
    this.value = this.data;
  }


  ngOnInit() {
    this.url = "Upload a file";
    this.createBannerForm();
  }
  createBannerForm() {
    if (this.value.new) {
      this.bannerForm = this.fb.group({
        banner_url: ['', Validators.required],
        description: ['']
      });
    } else {
      if(this.value.edit_banner.banner_url.url) {
      this.url = this.value.edit_banner.banner_url.url.split('/').pop();
      }
      this.bannerForm = this.fb.group({
        banner_url: [this.value.edit_banner.banner_url.url, Validators.required],
        description: [this.value.edit_banner.description]
      });
    }

  }
  onChangeImage(file: File) {
    this.image = file;
  }

  submit() {
    Object.keys(this.bannerForm.controls).forEach((key) => {
      console.log(typeof(this.bannerForm.get(key).value))
      if(typeof(this.bannerForm.get(key).value) == 'string') {
      this.bannerForm.get(key).setValue(this.bannerForm.get(key).value.trim())
      }
    });
    let data = this.bannerForm.value;
    data.banner_url = this.image;
    data.created_by_id = localStorage.getItem('user_id');
    const banner: FormData = new FormData();
    if (this.image) {
      banner.append('banner[banner_url]', data.banner_url);
    }
    banner.append('banner[description]', data.description);
    banner.append('banner[created_by_id]', data.created_by_id);
    console.log(banner);
    if (this.bannerForm.invalid) {
      this.toast.info('Please Upload Image');
      return;
    }
    this.clicked = true;
    if (this.value.new) {
      this.banner.banner_create(banner).pipe(untilDestroyed(this)).subscribe(res => {
        // this.ngOnInit();
        this.clicked = false;
        this.dialogRef.close();
        this.toast.success('Create Successfully');
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    } else {
      this.banner.banner_put(banner, this.value.banner_id).pipe(untilDestroyed(this)).subscribe(res => {
        // this.ngOnInit();
        this.clicked = false;
        this.dialogRef.close();
        this.toast.success('Updated Successfully');
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    }

  }
  ngOnDestroy() { }

}
