import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { CustomerService, SharedService } from 'app/services';

@Component({
  selector: 'app-masters',
  templateUrl: './masters.component.html',
  styleUrls: ['./masters.component.scss']
})
export class MastersComponent implements OnInit {

  categories: any;
  sub_categories: any;

  category_dataSource = new MatTableDataSource();
  category_displayedColumns: string[] = ['position', 'name'];
  sub_category_dataSource = new MatTableDataSource();
  sub_category_displayedColumns: string[] = ['position', 'name'];

  @ViewChild('category_sort', { static: true }) category_sort: MatSort;
  @ViewChild('sub_category_sort', { static: true }) sub_category_sort: MatSort;

  constructor(private customer: CustomerService, private sharedService: SharedService) { }

  ngOnInit() {
    this.getCategories();
  }
  select(tabGroup: { index: number }) {
    if (tabGroup.index === 0) {
      this.getCategories();
    } else {
      this.getSubCategories();
    }
  }

  getCategories() {
    this.customer.categories_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.categories = res;
      this.sharedService.setLoaderShownProperty(false);
      this.category_dataSource = new MatTableDataSource(this.categories);
      this.category_dataSource.sort = this.category_sort;
    });
  }
  getSubCategories() {
    this.customer.sub_categories().pipe(untilDestroyed(this)).subscribe(res => {
      this.sub_categories = res;
      this.sub_category_dataSource = new MatTableDataSource(this.sub_categories);
      this.sub_category_dataSource.sort = this.sub_category_sort;
    });
  }
  ngOnDestroy() { }

}
