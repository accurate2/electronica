import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { MastersComponent } from './masters.component';
import { SharedModule } from 'app/shared';

const routes: Routes = [{ path: '', component: MastersComponent }];

@NgModule({
  declarations: [MastersComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MastersModule { }
