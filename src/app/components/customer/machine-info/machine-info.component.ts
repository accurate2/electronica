import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CustomerService, SharedService } from 'services/app';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-machine-info',
  templateUrl: './machine-info.component.html',
  styleUrls: ['./machine-info.component.scss']
})
export class MachineInfoComponent implements OnInit {

  machineId: any;
  machines: any;
  list: any;

  displayedColumns: string[] = ['key', 'value'];
  dataSource = new MatTableDataSource();

  constructor(private route: ActivatedRoute, private customer: CustomerService, private sharedService: SharedService) { }

  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.machineId = params['id'];
      });
    console.log(this.machineId);
    this.customer.machine_getById(this.machineId).pipe(untilDestroyed(this)).subscribe(res => {
      this.machines = res;
      this.list = [
        { key: 'Make', value: this.machines.make },
        { key: 'Model', value: this.machines.mac_model },
        { key: 'Sl_No', value: this.machines.sl_no },
        { key: 'Si_circle', value: this.machines.si_circle },
        { key: 'Capacity', value: this.machines.capacity.value},
        { key: 'Accuracy', value: this.machines.accuracy.value},
        { key: 'Class', value: this.machines.machine_class.class_name },
        { key: 'Stamp Quarter', value: this.machines.stamp_quarters},
        { key: 'Stamping City', value: this.machines.stamping_city},
      ]
      console.log(this.machines);
      this.dataSource = new MatTableDataSource(this.list);
      this.sharedService.setLoaderShownProperty(false);
    });
  }
  ngOnDestroy() { }

}
