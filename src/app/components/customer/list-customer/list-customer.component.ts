import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerService, SharedService, ReportService, DealerService } from 'services/app';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { MatTableDataSource, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { TokenService } from 'services/core';
import { saveAs } from 'file-saver';
import { debounceTime, throttle, throttleTime } from 'rxjs/operators';


@Component({
  selector: 'app-list-customer',
  templateUrl: './list-customer.component.html',
  styleUrls: ['./list-customer.component.scss']
})
export class ListCustomerComponent implements OnInit {

  pageNo: any;
  customers: any;
  customer_scroll: any;
  searchValue = "";
  customerID: any[] = [];
  dealer = "";
  si_circle = "";
  state = "";
  city = "";
  area = "";
  panelOpenState = false;
  dealers: any;
  si_circleList: any;
  states: any;
  cities: any;
  dealer_list: any;
  si_circle_list: any;
  state_list: any;
  city_list: any;
  stateId: any;
  dealer_input = "";
  si_input = "";
  state_input = "";
  city_input = "";


  displayedColumns: string[] = ['select', 'position', 'user.uuid', 'user.full_name', 'user.username', 'state.name', 'city.name', 'customer_category', 'assigned_to','qrcode', 'dealer_name', 'action'];
  dataSource = new MatTableDataSource();
  selection = new SelectionModel(true, []);

  pdf: any;
  currentDatetime: any;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  toggle() {
    this.panelOpenState = !this.panelOpenState
  }
  sortingDataAccessor(item, property) {
    if (property.includes('.')) {
      return property.split('.')
        .reduce((object, key) => object[key], item);
    }
    return item[property];
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  constructor(private customer: CustomerService, private toast: ToastrService, public router: Router, public token: TokenService, private sharedService: SharedService, private report: ReportService, private dealer_service: DealerService) { }

  ngOnInit() {
    this.sharedService.setLoaderShownProperty(true);
    this.getCustomers();
    this.getDealers();
    this.getStates();
    this.getCities();
    this.get_si_circle_list();
    if (this.token.getRole() === 'Verifier') {
      this.displayedColumns = ['select', 'position', 'user.uuid', 'user.full_name', 'user.username', 'address_line1', 'state.name', 'city.name', 'customer_category', 'qrcode', 'dealer_name', 'action'];
    }
  }
  getCustomers() {
    this.pageNo = 1;
    this.searchValue = "";
    this.customer.customer_get(this.pageNo, this.dealer, this.state, this.city, this.area, this.si_circle).pipe(untilDestroyed(this)).subscribe(res => {
      this.customers = res;
      this.dataSource = new MatTableDataSource(this.customers);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }
  applyFilter() {
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.customer.customer_filter(this.pageNo, this.searchValue, this.dealer, this.state, this.city, this.area, this.si_circle).pipe(untilDestroyed(this)).subscribe(res => {
        this.customers = res;
        this.dataSource = new MatTableDataSource(this.customers);
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.dataSource.sort = this.sort;
      });
    } else {
      this.getCustomers();
    }
  }

  delete(id) {
    Swal.fire({
      title: 'Are you sure want to delete?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this.customer.customer_delete(id).pipe(untilDestroyed(this)).subscribe(res => {
          this.toast.success('Deleted Successfully');
          this.ngOnInit();
        }, error => {
          this.toast.error('Something Went Wrong');
        });
      }
    });
  }
  edit(id) {
    this.sharedService.updateMessage(true);
    this.router.navigate(['/customer/edit/' + id]);
  }

  onScroll() {
    this.pageNo = this.pageNo + 1;
    this.customer.customer_filter(this.pageNo, this.searchValue, this.dealer, this.state, this.city, this.area, this.si_circle).subscribe(res => {
      this.customer_scroll = res;
      this.customer_scroll.map(item => this.customers.push(item));
      this.dataSource = new MatTableDataSource(this.customers);
      this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
      this.dataSource.sort = this.sort;
    })
  }
  selectAll(event) {
    console.log(event);
    if (event.checked) {
      this.customerID = [];
    }
    this.customers.map(item => {
      if (item.all_qr_available) {
        this.selectCustomer(item.id, event)
      }
    });
  }

  selectCustomer(cus_id, event) {
    console.log(cus_id, event.checked);
    if (event.checked) {
      this.customerID.push(cus_id);
    } else {
      const index: number = this.customerID.indexOf(cus_id);
      console.log(index);
      if (index > -1) {
        this.customerID.splice(index, 1);
      }
    }
    console.log(this.customerID);
  }
  download_qrcode() {
    let data = {
      customer_ids: this.customerID
    }
    if (this.customerID.length === 0) {
      // this.toast.info('You must select atleast one Customer');
    } else {
      this.customer.download_qrCode(data).pipe(untilDestroyed(this)).subscribe(res => {
        this.pdf = res;
        if (res.type === 'application/json') {
          var reader = new FileReader();
          reader.readAsText(res);
          reader.onload = () => {
            var readResult = JSON.parse(reader.result.toString());
            this.toast.warning(readResult.message);
          };
        } else {
          const blob = new Blob([this.pdf], { type: res.type });
          this.currentDatetime = new Date().toISOString().split('T')[0];
          const filename = 'Machine' + this.currentDatetime + '.pdf';
          saveAs(blob, filename);
          this.toast.success('Downloaded Successfully');
          // const url= window.URL.createObjectURL(blob);
          // window.open(url,filename);
        }
        this.selection.clear();
        this.customerID = [];
      }, error => {
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    }
  }
  getDealers() {
    this.report.dealers_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res)
      this.dealers = res;
      this.dealer_list = res;
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_dealer(event) {
    let value = event.trim().toLowerCase();
    this.dealers = this.dealer_list.filter(dealer => dealer.full_name.toLowerCase().indexOf(value) > -1);
    if (this.dealers.length === 0) {
      this.dealer = "";
    }
  }
  
  get_si_circle_list() {
    this.report.si_circle_get().pipe(untilDestroyed(this)).subscribe(res => {
      this.si_circleList = res.data;
      this.si_circle_list = res.data;
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  
  search_si(event) {
    let value = event.trim().toLowerCase();
    this.si_circleList = this.si_circle_list.filter(si => si.toLowerCase().indexOf(value) > -1);
    if (this.si_circleList.length === 0) {
      this.si_circle = "";
    }
  }

  getStates() {
    this.report.state_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.states = res;
      this.state_list = res;
    });
  }
  search_state(event) {
    let value = event.trim().toLowerCase();
    this.states = this.state_list.filter(state => state.name.toLowerCase().indexOf(value) > -1);
    if (this.states.length === 0) {
      this.state = "";
    }
  }
  getCities(event?) {
    this.city = "";
    this.city_input = "";
    this.stateId = event;
    this.report.city_get(this.stateId).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.cities = res;
      this.city_list = res;
    });
  }
  search_city(event) {
    let value = event.trim().toLowerCase();
    this.cities = this.city_list.filter(city => city.name.toLowerCase().indexOf(value) > -1);
    if (this.cities.length === 0) {
      this.city = "";
    }
  }
  filter() {
    this.pageNo = 1;
    if (this.searchValue.length > 0) {
      this.customer.customer_filter(this.pageNo, this.searchValue, this.dealer, this.state, this.city, this.area, this.si_circle).pipe(untilDestroyed(this)).subscribe(res => {
        this.customers = res;
        this.dataSource = new MatTableDataSource(this.customers);
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
        this.dataSource.sort = this.sort;
        this.sharedService.setLoaderShownProperty(false);
      }, error => {
        this.toast.error('Something Went Wrong');
      });
    } else {
      this.getCustomers();
    }
  }
  clear() {
    this.dealer = "";
    this.si_circle = "";
    this.state = "";
    this.city = "";
    // this.cities = [];
    this.dealer_input = "";
    this.si_input = "";
    this.state_input = "";
    this.city_input = "";
    this.ngOnInit();
  }

  ngOnDestroy() { }

}
