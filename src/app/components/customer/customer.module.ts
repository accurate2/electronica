import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ListCustomerComponent } from './list-customer/list-customer.component';
import { EditCustomerComponent, customer_product_dialog_box } from './edit-customer/edit-customer.component';
import { SharedModule } from 'app/shared';
import { MachineInfoComponent } from './machine-info/machine-info.component';


const routes: Routes = [{
  path: '',
  children: [{
    path: 'list',
    component: ListCustomerComponent
  },
  {
    path: 'edit/:id',
    component: EditCustomerComponent
  },
  {
    path: 'machine_info/:id',
    component: MachineInfoComponent
  }]
}];

@NgModule({
  declarations: [ListCustomerComponent, EditCustomerComponent, customer_product_dialog_box, MachineInfoComponent],
  entryComponents: [ customer_product_dialog_box ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]

})
export class CustomerModule { }
