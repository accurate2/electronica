import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CustomerService, ProductService, SharedService, DealerService, ReportService } from 'services/app';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { untilDestroyed } from 'ngx-take-until-destroy';
export interface Customer_Product_Dialog_Data { }
import { environment } from 'environments/environment';
import Swal from 'sweetalert2';
import { TokenService } from 'services/core';
declare var $: any;



@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.scss']
})
export class EditCustomerComponent implements OnInit {

  customerForm: FormGroup;
  customers: any;
  customerId: any;
  BaseURL: string = environment.imageUrl;
  states: any;
  cities: any;
  stateId: any;
  state_list: any;
  city_list: any;
  categories: any;
  sub_categories: any;
  categoryId: any;
  clicked = false;
  category: any;
  sub_category: any;


  constructor(private fb: FormBuilder, private toast: ToastrService, public dialog: MatDialog, private customer: CustomerService,
    private route: ActivatedRoute, private router: Router, private sharedService: SharedService, public token: TokenService, private report: ReportService) { }

  ngOnInit() {
    this.getCustomers();
    this.createCustomerForm();
    this.getStates();
    this.getCategories();
  }
  getCustomers() {
    this.route.params
      .subscribe((params: Params) => {
        this.customerId = params['id'];
      });
    this.customer.customer_getById(this.customerId).pipe(untilDestroyed(this)).subscribe(res => {
      this.customers = res;
      this.sharedService.setLoaderShownProperty(false);
      this.getCities(this.customers.state.id);
      this.category = this.customers.customer_category !== null ? this.customers.customer_category.id : "";
      this.sub_category = this.customers.sub_category !== null ? this.customers.sub_category.id : "";
      this.getSubCategories(this.category);
      this.customerForm
        .patchValue({
          uuid: this.customers.user.uuid,
          full_name: this.customers.user.full_name,
          username: this.customers.user.username,
          status: this.customers.user.status,
          customer_attributes: {
            id: this.customers.id,
            business_name: this.customers.business_name,
            whatsapp_no: this.customers.whatsapp_no,
            state: this.customers.state.id,
            city: this.customers.city.id,
            pincode: this.customers.pincode,
            address_line1: this.customers.address_line1,
            customer_category_id: this.category,
            sub_category_id: this.sub_category,
            si_circle: this.customers.si_circle 
          }
        });
    });
  }
  add() {
    const dialogRef = this.dialog.open(customer_product_dialog_box, {
      width: 'auto',
      data: { new: 'new', customer_id: this.customerId }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getCustomers();
    });
  }
  edit(machine, id) {
    const dialogRef = this.dialog.open(customer_product_dialog_box, {
      width: 'auto',
      data: { edit_machine: machine, machine_id: id }
    });
    dialogRef.afterClosed().subscribe(() => {
      this.getCustomers();
    });
  }

  createCustomerForm() {
    this.customerForm = this.fb.group({
      uuid: ['', Validators.required],
      full_name: ['', Validators.required],
      username: ['', Validators.required],
      status: [''],
      customer_attributes: this.fb.group({
        id: [''],
        business_name: ['', Validators.required],
        whatsapp_no: [''],
        state: ['', Validators.required],
        city: ['', Validators.required],
        pincode: ['', Validators.required],
        address_line1: ['', Validators.required],
        customer_category_id: ['', Validators.required],
        sub_category_id: ['', Validators.required],
        si_circle: ['']
      })
    });
  }

  generate_qrcode(id) {
    this.customer.generate_qrCode(id).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.getCustomers();
    }, error => {
      this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
    });
  }

  getStates() {
    this.report.state_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.states = res;
      this.state_list = res;
    });
  }
  search_state(event) {
    let value = event.trim().toLowerCase();
    this.states = this.state_list.filter(state => state.name.toLowerCase().indexOf(value) > -1);
    if (this.states.length === 0) {
      this.customerForm.patchValue({
        customer_attributes: ({
          state: ""
        })
      })
    }
  }
  getCities(event) {
    this.customerForm.patchValue({
      customer_attributes: ({
        city: ""
      })
    })
    this.stateId = event;
    this.report.city_get(this.stateId).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.cities = res;
      this.city_list = res;
    });
  }
  search_city(event) {
    let value = event.trim().toLowerCase();
    this.cities = this.city_list.filter(city => city.name.toLowerCase().indexOf(value) > -1);
    if (this.cities.length === 0) {
      this.customerForm.patchValue({
        customer_attributes: ({
          city: ""
        })
      })
    }
  }
  getCategories() {
    this.customer.categories_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.categories = res;
    });
  }
  getSubCategories(event?) {
    this.customerForm.patchValue({
      customer_attributes: ({
        sub_category_id: ""
      })
    })
    this.categoryId = event;
    this.customer.sub_categories_get(this.categoryId).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.sub_categories = res;
    });
  }


  submit() {
    Object.keys(this.customerForm.controls).forEach((key) => {
      console.log(typeof(this.customerForm.get(key).value))
      if(typeof(this.customerForm.get(key).value) == 'string' ) {
      this.customerForm.get(key).setValue(this.customerForm.get(key).value.trim())
      } 
    });
    Object.keys(this.customerForm.value.customer_attributes).forEach((key) => {
      console.log(typeof(this.customerForm.controls.customer_attributes.get(key).value))
      if(typeof(this.customerForm.controls.customer_attributes.get(key).value) == 'string' ) {
        this.customerForm.controls.customer_attributes.get(key).setValue(this.customerForm.controls.customer_attributes.get(key).value.trim())
      } 
    });
    if (this.customerForm.invalid) {
      return;
    }
    this.clicked = true;
    let data = this.customerForm.value;
    if (this.customers.user.status === null) {
      if (this.customerForm.value.status) {
        data.status = "Verified";
      } else {
        data.status = null;
      }
    }
    console.log(data)
    if (this.customers.user.status === data.status) {
      this.customer.customer_put(data, this.customers.user.id).pipe(untilDestroyed(this)).subscribe(res => {
        this.ngOnInit();
        this.clicked = false;
        this.toast.success('Updated Successfully');
        this.router.navigateByUrl('/customer/list');
      }, error => {
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    } else {
      Swal.fire({
        title: 'Are you sure to verify this customer?',
        text: "You will not be able to modify this once you have done!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          this.customer.customer_put(data, this.customers.user.id).pipe(untilDestroyed(this)).subscribe(res => {
            this.ngOnInit();
            this.clicked = false;
            this.toast.success('Updated Successfully');
            this.router.navigateByUrl('/customer/list');
          }, error => {
            this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
          });
        }
      });
    }
  }
  ngOnDestroy() { }

}

//Customer Machine Details

@Component({
  selector: 'customer_product_dialog_box',
  templateUrl: 'customer_product_dialog_box.html',
})

export class customer_product_dialog_box {

  value: any;
  productForm: FormGroup;
  pageNo: any;
  capacities: any;
  accuracies: any;
  machine_class: any;
  image: any[] = [];
  searchValue: any;
  url: any;
  capacityId: any;
  accuracyId: any;
  capacity: any;
  accuracy: any;
  searchText: any;
  clicked = false;


  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<customer_product_dialog_box>, @Inject(MAT_DIALOG_DATA) public data: Customer_Product_Dialog_Data,
    private toast: ToastrService, private customer: CustomerService, private product: ProductService) {
    this.value = this.data;
  }

  ngOnInit() {
    this.url = "Upload a file";
    this.getCapacities();
    if (!this.value.new) {
      this.capacityId = this.value.edit_machine.machine_class.capacity;
      this.accuracyId = this.value.edit_machine.machine_class.accuracy;
      this.getAccuraciesByCapacity(this.capacityId);
      this.getMachineBycapacity(this.capacityId, this.accuracyId);
    }
    this.createProductForm();
  }

  createProductForm() {
    // if (this.value.edit_machine.upcoming_due.verification_cert_url) {
    //   this.url = this.value.edit_machine.upcoming_due.verification_cert_url.split('/').pop();
    // }
    if (this.value.new) {
      this.productForm = this.fb.group({
        make: ['', Validators.required],
        mac_model: ['', Validators.required],
        capacity_id: ['', Validators.required],
        accuracy_id: ['', Validators.required],
        machine_class_id: ['', Validators.required],
        sl_no: ['', Validators.required],
        stamp_quarters: ['', Validators.required],
        stamping_city: ['', Validators.required],
        // si_circle: ['', Validators.required],
        customer_id: [this.value.customer_id],
        machine_url: [''],
        stamping_dues_attributes: this.fb.group({
          stamp_due_date: ['', Validators.required],
        })
      });
    } else {
      this.productForm = this.fb.group({
        id: [this.value.machine_id],
        make: [this.value.edit_machine.make, Validators.required],
        mac_model: [this.value.edit_machine.mac_model, Validators.required],
        capacity_id: [this.value.edit_machine.machine_class.capacity, Validators.required],
        accuracy_id: [this.value.edit_machine.machine_class.accuracy, Validators.required],
        machine_class_id: [this.value.edit_machine.machine_class.class_name, Validators.required],
        sl_no: [this.value.edit_machine.sl_no, Validators.required],
        stamp_quarters: [this.value.edit_machine.stamp_quarters, Validators.required],
        stamping_city: [this.value.edit_machine.stamping_city, Validators.required],
        // si_circle: [this.value.edit_machine.si_circle, Validators.required],
        stamping_dues_attributes: this.fb.group({
          id: [this.value.edit_machine.upcoming_due.id],
          stamp_due_date: [{ value: this.value.edit_machine.upcoming_due.stamp_due_date, disabled: true }, Validators.required],
          // verification_cert_no: [this.value.edit_machine.upcoming_due.verification_cert_no],
          // verification_cert_url: [this.value.edit_machine.upcoming_due.verification_cert_url]
        })
      });
    }
  }


  getCapacities() {
    this.pageNo = 1;
    this.product.capacity_get(this.pageNo).pipe(untilDestroyed(this)).subscribe(res => {
      this.capacities = res.data;
      this.capacity = res.data;
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_Capacity(event) {
    let value = event.trim().toLowerCase();
    this.capacities = this.capacity.filter(capacity => capacity.toLowerCase().indexOf(value) > -1);
  }

  onChangeImage(file) {
    console.log(file);
    for(let file1 of file) {
      console.log(file1)
      this.image.push(file1);
    }
    console.log(this.image)

    // if (file != undefined) {
    //   var machine: FormData = new FormData();
    //   machine.append('picture', JSON.stringify( this.image));
    //   this.customer.image_upload(machine).pipe(untilDestroyed(this)).subscribe(res => {
    //     if (res.pic_url) {
    //       this.image = res.pic_url;
    //     }
    //     this.toast.success('Image Uploaded Successfully');
    //   });
    // }
  }

  getAccuraciesByCapacity(capacity) {
    this.capacityId = capacity;
    this.product.accuracies_getByCapacity(capacity).pipe(untilDestroyed(this)).subscribe(res => {
      this.accuracies = res.data;
      this.accuracy = res.data;
      this.onChangeAccuracy(this.accuracies[0]);
      this.productForm
        .patchValue({
          accuracy_id: this.accuracies[0]
        });
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_Accuracy(event) {
    let value = event.trim().toLowerCase();
    this.accuracies = this.accuracy.filter(accuracy => accuracy.toLowerCase().indexOf(value) > -1);
  }
  onChangeAccuracy(val) {
    this.accuracyId = val;
    this.getMachineBycapacity(this.capacityId, this.accuracyId);
  }

  getMachineBycapacity(capacity, accuracy) {
    this.product.machine_class_getByCapacity(capacity, accuracy).pipe(untilDestroyed(this)).subscribe(res => {
      this.machine_class = res;
      this.productForm
        .patchValue({
          machine_class_id: this.machine_class.class_name
        });
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  sl_no_validation(val) {
    if (!this.value.new && this.value.edit_machine.sl_no !== val) {
      this.customer.sl_no_validation(val).pipe(untilDestroyed(this)).subscribe(res => {
        if (!res.status) {
          // this.toast.error(res.message);
          this.productForm.controls['sl_no'].setErrors({ sl_no_valid: true });
        }
      });
    } else {
      this.customer.sl_no_validation(val).pipe(untilDestroyed(this)).subscribe(res => {
        if (!res.status) {
          // this.toast.error(res.message);
          this.productForm.controls['sl_no'].setErrors({ sl_no_valid: true });
        }
      });
    }
  }

  submit() {
    Object.keys(this.productForm.controls).forEach((key) => {
      console.log(typeof(this.productForm.get(key).value))
      if(typeof(this.productForm.get(key).value) == 'string' ) {
      this.productForm.get(key).setValue(this.productForm.get(key).value.trim())
      } 
    });
    if (this.productForm.invalid) {
      return;
    }
    this.clicked = true;
    let data = this.productForm.value;
    data.make = data.make.toUpperCase();
    data.machine_class_id = this.machine_class.id;
    console.log(data)
    if (this.value.new) {
      data.distributor_id = localStorage.getItem('user_id');
      if (this.image) {
        data.machine_url = this.image;
      }
      var machine: FormData = new FormData();
      this.image.map((image,index) => {
        console.log(index)
        machine.append('machine[machine_assets_attributes]'+index +'[pic_url]', image);
      });
      machine.append('machine[make]', data.make);
      machine.append('machine[mac_model]', data.mac_model);
      machine.append('machine[capacity_id]', data.capacity_id);
      machine.append('machine[accuracy_id]', data.accuracy_id);
      machine.append('machine[machine_class_id]', data.machine_class_id);
      machine.append('machine[sl_no]', data.sl_no);
      machine.append('machine[stamp_quarters]', data.stamp_quarters);
      machine.append('machine[stamping_city]', data.stamping_city);
      // machine.append('machine[si_circle]', data.si_circle);
      machine.append('machine[customer_id]', data.customer_id)
      machine.append('machine[distributor_id]', data.distributor_id)
      machine.append('machine[stamping_dues_attributes][0][customer_id]', data.customer_id);
      machine.append('machine[stamping_dues_attributes][0][stamp_due_date]', data.stamping_dues_attributes.stamp_due_date);
      machine.append('machine[stamping_dues_attributes][0][status]', "Open");
      machine.append('machine[stamping_dues_attributes][0][distributor_id]', data.distributor_id);
      machine.forEach((value, key) => {
        console.log(key + value)
      });
      
      this.customer.machine_create(machine).pipe(untilDestroyed(this)).subscribe(res => {
        this.clicked = false;
        this.toast.success('Created Successfully');
        // this.ngOnInit();
        this.dialogRef.close();
      }, error => {
        this.clicked = false;        
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    } else {
      this.customer.machine_put(data, this.value.machine_id).pipe(untilDestroyed(this)).subscribe(res => {
        this.clicked = false;        
        this.toast.success('Updated Successfully');
        // this.ngOnInit();
        this.dialogRef.close();
      }, error => {
        this.clicked = false;
        this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));
      });
    }
  }

  ngOnDestroy() { }

}
