import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from 'services/app';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  hide = true;
  login_value: any;
  clicked = false;


  constructor(public router: Router, private fb: FormBuilder, private toast: ToastrService,
               public log: LoginService) { }

  ngOnInit() {
    this.createLoginForm();
  }
  createLoginForm() {
    this.loginForm = this.fb.group ({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }
  login() {
    Object.keys(this.loginForm.controls).forEach((key) => {
      console.log(typeof(this.loginForm.get(key).value))
      if(typeof(this.loginForm.get(key).value) == 'string') {
      this.loginForm.get(key).setValue(this.loginForm.get(key).value.trim())
      }
    });
    if (this.loginForm.invalid) {
      // this.toast.info('Enter Email and Password');
      return;
  }
  this.clicked = true;
  this.log.login(this.loginForm.value).pipe(untilDestroyed(this)).subscribe(res => {
    this.login_value = res;
    console.log(this.login_value);
    localStorage.setItem('token', this.login_value.token);
    localStorage.setItem('user_id', this.login_value.id);
    localStorage.setItem('role', this.login_value.role.role_name);
    localStorage.setItem('user_name', this.login_value.user.full_name);
    this.router.navigateByUrl('/dashboard');
    this.toast.success('Logged In Successfully');
    const savedToken = localStorage.getItem('token');
    console.log(savedToken);
    this.clicked = false;
  }, error => {
    this.clicked = false;
    this.toast.error('Login Failed');
  });
  }
  spaceNotallowed(event) {
    if (event.keyCode === 32 ) {
      return false;
    }
  }
  ngOnDestroy() {}

}
