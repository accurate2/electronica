import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserService, SharedService } from 'services/app';
import { untilDestroyed } from 'ngx-take-until-destroy'
import { Router } from '@angular/router';
import {MustMatch} from 'app/components/admin';


@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  forgetForm: FormGroup;
  verifyForm: FormGroup;
  verifyOTP = false;
  hide = true;
  hide1 = true;
  clicked = false;
  clicked1 = false;


  constructor(private fb: FormBuilder, private toast: ToastrService, private user: UserService, public router: Router, private sharedService: SharedService) { }


  ngOnInit() {
    this.createForgetForm();
    this.createVerifyForm();
  }
  createForgetForm() {
    this.forgetForm = this.fb.group({
      email_id: ['', Validators.required]
    });
  }

  createVerifyForm() {
    this.verifyForm = this.fb.group({
      verification_code: ['', Validators.required],
      password: ['', Validators.required],
      password_confirmation: ['', Validators.required]
    }, { validator: MustMatch('password', 'password_confirmation') 
    });
  }
  submit() {
    if (this.forgetForm.invalid) {
      return;
    }
    console.log('send')
    this.clicked = true;
    this.user.forgot_password(this.forgetForm.value.email_id).pipe(untilDestroyed(this)).subscribe(res => {
      this.clicked = false;
      if(res.status) {
        this.toast.success(res.message);
        localStorage.setItem('user_id', res.user_id);
        this.verifyOTP = true;
        document.getElementById('color').setAttribute('readOnly', 'readOnly');
      } else {
        this.toast.error(res.message);
      }
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      this.clicked = false;
      this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));      
    });
  }
  verify() {
    if (this.verifyForm.invalid) {
      // this.toast.info('Please Enter all fields');
      return;
    }
    this.clicked1 = true;
    let data = this.verifyForm.value;
    data.id = localStorage.getItem('user_id');
    console.log(data);
    this.user.otp_verification(data).pipe(untilDestroyed(this)).subscribe(res => {
      this.clicked1 = false;     
      if (res.status) {
      this.toast.success('Password Changed Successfully');
      this.router.navigateByUrl('');

      } else {
      this.toast.error(res.message);
      }
    }, error => {
      this.clicked1 = false;
      this.toast.error(Object.keys(error.error) + ' ' + Object.values(error.error));     
    });
  }
  spaceNotallowed(event) {
    if (event.keyCode === 32 ) {
      return false;
    }
  }
  ngOnDestroy() { }
}


