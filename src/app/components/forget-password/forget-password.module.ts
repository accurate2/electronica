import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { ForgetPasswordComponent } from './forget-password.component';
import { SharedModule } from 'app/shared';

const routes: Routes = [{ path: '', component: ForgetPasswordComponent }];

@NgModule({
  declarations: [ForgetPasswordComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ForgetPasswordModule { }




