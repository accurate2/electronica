import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReportService, SharedService } from 'services/app';
import { ToastrService } from 'ngx-toastr';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { TokenService } from 'services/core';
import 'jspdf-autotable';







@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  reportForm: FormGroup;
  dealers: any;
  reports: any;
  reportExcel: any[] = [];
  reportType: any;
  si_circleList: any;
  dealer: any;
  si_circle: any;
  panelOpenState = false;
  maxDate = new Date();
  searchValue =  "";
  cities: any;
  city_list: any;
  service_list: any;
  service: any;
  customers: any;
  customer_list: any;
  models: any;
  model_list: any;




  displayedColumns: string[] = ['position', 'user.uuid', 'user.full_name', 'user.username', 'customer.city.name', 'machine.mac_model',
    'service_type.type_name', 'status'];
  dataSource = new MatTableDataSource();

  @ViewChild('TABLE', { static: true }) table: ElementRef;

  constructor(private fb: FormBuilder, private report: ReportService, private toast: ToastrService, public datepipe: DatePipe, public token: TokenService, private sharedService: SharedService) { }

  toggle() {
    this.panelOpenState = !this.panelOpenState
  }

  ngOnInit() {
    this.ReportFilterForm();
    this.getDealersID();
    this.getCustomers();
    this.get_si_circle_list();
    this.getCities();
    this.getServices();
    this.getModels();

  }


  ReportFilterForm() {
    var begin = new Date();
    var end = new Date();
    begin.setDate(begin.getDate() - 30);
    end.setDate(end.getDate() - 1);
    this.reportForm = this.fb.group({
      date: [{
        begin: this.datepipe.transform(begin, 'yyyy-MM-dd'),
        end: this.datepipe.transform(end, 'yyyy-MM-dd')
      }, Validators.required],
      dealer_id: [''],
      customer_id: [''],
      city_id: [''],
      service_type: [''],
      mac_model: [''],
      si_circle: [''],
      reportType: ['Service', Validators.required],
      dealer_input: [],
      customer_input: [],
      city_input: [],
      service_input: [],
      model_input: [],
      si_input: [],
    });
    this.submit();
  }
  getDealersID(event?) {
    this.reportForm.patchValue({
      dealer_id: "",
      dealer_input: ""
    })
    this.report.dealers_get(event).pipe(untilDestroyed(this)).subscribe(res => {
      this.dealer = res;
      this.dealers = res;
      this.sharedService.setLoaderShownProperty(false);
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }

  get_si_circle_list() {
    this.report.si_circle_get().pipe(untilDestroyed(this)).subscribe(res => {
      this.si_circleList = res.data;
      this.si_circle = res.data;
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_dealer(event) {
    let value = event.trim().toLowerCase();
    this.dealers = this.dealer.filter(dealer => dealer.full_name.toLowerCase().indexOf(value) > -1
      || dealer.uuid.toLowerCase().indexOf(value) > -1);
    if (this.dealers.length === 0) {
      this.reportForm.patchValue({
        dealer_id: ""
      })
    }
  }
  search_si(event) {
    let value = event.trim().toLowerCase();
    this.si_circleList = this.si_circle.filter(si => si.toLowerCase().indexOf(value) > -1);
    if (this.si_circleList.length === 0) {
      this.reportForm.patchValue({
        si_circle: ""
      })
    }
  }
  getCustomers(event?) {
    this.reportForm.patchValue({
      customer_id: "",
      customer_input: ""
    })
    this.report.customers_get(event).pipe(untilDestroyed(this)).subscribe(res => {
      this.customers = res;
      this.customer_list = res;
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_customer(event) {
    let value = event.trim().toLowerCase();
    this.customers = this.customer_list.filter(customer => customer.full_name.toLowerCase().indexOf(value) > -1);
    if (this.customers.length === 0) {
      this.reportForm.patchValue({
        customer_id: ""
      })
    }
  }
  getCities() {
    this.report.city_get().pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.cities = res;
      this.city_list = res;
    });
  }
  search_city(event) {
    let value = event.trim().toLowerCase();
    this.cities = this.city_list.filter(city => city.name.toLowerCase().indexOf(value) > -1);
    if (this.cities.length === 0) {
      this.reportForm.patchValue({
        city_id: ""
      })
    }
  }
  getServices() {
    this.report.service_get().pipe(untilDestroyed(this)).subscribe(res => {
      this.service_list = res;
      this.service = res;
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }
  search_service(event) {
    let value = event.trim().toLowerCase();
    this.service_list = this.service.filter(service => service.type_name.toLowerCase().indexOf(value) > -1);
    if (this.service_list.length === 0) {
      this.reportForm.patchValue({
        service_type: ""
      })
    }
  }
  getModels() {
    this.report.model_get().pipe(untilDestroyed(this)).subscribe(res => {
      this.models = res.data;
      console.log(this.models)
      this.model_list = res.data;
    });
  }
  search_model(event) {  
    let value = event.trim().toLowerCase();
    this.models = this.model_list.filter(model => model.toLowerCase().indexOf(value) > -1);
    if (this.models.length === 0) {
      this.reportForm.patchValue({
        mac_model: ""
      })
    }
  }
  clear() {
    this.ngOnInit();
  }
  applyFilter() {
    console.log(this.searchValue)
    let data = this.reportForm.value;
    data.fromDate = this.datepipe.transform(data.date.begin, 'yyyy-MM-dd');
    data.toDate = this.datepipe.transform(data.date.end, 'yyyy-MM-dd');
    if (this.searchValue.length > 0) {
      this.report.report_search(data.reportType, data.fromDate, data.toDate, data.dealer_id, data.customer_id, data.city_id, data.service_type, data.mac_model, data.si_circle, this.searchValue).pipe(untilDestroyed(this)).subscribe(res => {
        this.reports = res;
        console.log(this.reports.length)
        this.dataSource = new MatTableDataSource(this.reports);
      }, error => {
        this.toast.error('Something Went Wrong');
      });
    }
  }
  submit() {
    let data = this.reportForm.value;
    if (this.reportForm.invalid) {
      // this.toast.info('Please Enter All Fields');
      return;
    }
    this.reportType = data.reportType;
    if (data.reportType === 'Service') {
      if (this.token.getRole() === 'Distributor') {
        this.displayedColumns = ['position', 'user.uuid', 'user.full_name', 'user.username', 'customer.city.name', 'service_type.type_name', 'machine.mac_model', 'service_cost', 'status'];
      } else {
        this.displayedColumns = ['position', 'user.uuid', 'user.full_name', 'user.username', 'customer.city.name', 'service_type.type_name', 'machine.mac_model', 'status'];
      }
    } else {
      if (this.token.getRole() === 'Distributor') {
        this.displayedColumns = ['position', 'user.uuid', 'user.full_name', 'user.username', 'customer.city.name', 'machine.mac_model', 'stamping_cost', 'status'];
      } else {
        this.displayedColumns = ['position', 'user.uuid', 'user.full_name', 'user.username', 'customer.city.name', 'machine.mac_model', 'status'];
      }
    }

    data.fromDate = this.datepipe.transform(data.date.begin, 'yyyy-MM-dd');
    data.toDate = this.datepipe.transform(data.date.end, 'yyyy-MM-dd');
    console.log(data);
    this.report.report_get(data.reportType, data.fromDate, data.toDate, data.dealer_id, data.customer_id, data.city_id, data.service_type, data.mac_model, data.si_circle).pipe(untilDestroyed(this)).subscribe(res => {
      this.reports = res;
      console.log(this.reports.length)
      this.dataSource = new MatTableDataSource(this.reports);
    }, error => {
      this.toast.error('Something Went Wrong');
    });
  }

  exportExcel() {
    this.reportExcel = [];
    if (this.reportType === 'Service') {
      this.reports.map(item => this.reportExcel.push({
        Sl_NO: this.reportExcel.length + 1,
        ID: item.user.uuid,
        Customer_name: item.user.full_name,
        Mobile_no: item.user.username,
        City: item.customer.city.name,
        Service_type: item.service_type.type_name,
        Model: item.machine.mac_model,
        Price: item.service_cost,
        Status: item.status
      }));
    } else {
      this.reports.map(item => this.reportExcel.push({
        Sl_NO: this.reportExcel.length + 1,
        ID: item.user.uuid,
        Customer_name: item.user.full_name,
        Mobile_no: item.user.username,
        City: item.customer.city.name,
        Model: item.machine.mac_model,
        Price: item.stamping_cost,
        Status: item.status
      }));
    }
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.reportExcel);
    console.log('worksheet', ws);
    const wb: XLSX.WorkBook = { Sheets: { 'Report': ws }, SheetNames: ['Report'] };
    const excelBuffer: any = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    const data: Blob = new Blob([excelBuffer], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'
    });
    saveAs(data, 'report.xlsx');
    this.toast.success('Downloaded Successfully');
  }

  public convertPDF() {
    const doc = new jsPDF();
    doc.text(80, 10, this.reportType + " " + "Report", 'left');
    doc.autoTable({
      html: '#exportable', theme: 'grid', headStyles: {
        fillColor: [128, 128, 128]
      }
    });
    doc.save('report.pdf');
    //   doc.setProperties({
    //     title: "report"
    // });
    // doc.autoPrint();
    this.toast.success('Downloaded Successfully');
  }

  ngOnDestroy() { }

}
