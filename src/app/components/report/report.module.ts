import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';


import { ReportComponent } from './report.component';
import { SharedModule } from 'app/shared';

const routes: Routes = [{ path: '', component: ReportComponent }];


@NgModule({
  declarations: [ReportComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ReportModule { }
