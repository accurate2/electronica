import { Component, OnInit } from '@angular/core';
import { DashboardService, SharedService } from 'services/app';
import * as Highcharts from 'highcharts';
import { ToastrService } from 'ngx-toastr';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { DatePipe } from '@angular/common';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  analytics: any;
  customerChart: any;
  serviceChart: any;
  service: any;
  Highcharts: typeof Highcharts = Highcharts;
  stamps: any;
  renewals: any;
  machines: any;
  customers: any;
  dealers: any;
  customer_analytic: any;
  service_analytic: any;
  date: any;
  stamp_sd = "";
  stamp_ed = "";
  renew_sd = "";
  renew_ed = "";
  mach_sd = "";
  mach_ed = "";
  cus_sd = "";
  cus_ed = "";
  dealer_sd = "";
  dealer_ed = "";
  analytics_sd = "";
  analytics_ed = "";
  service_analytics_sd = "";
  service_analytics_ed = "";
  maxDate = new Date();




  constructor(private dashboard: DashboardService, private toast: ToastrService, private sharedService: SharedService, public datepipe: DatePipe) { }

  ngOnInit() {
    this.sharedService.setLoaderShownProperty(true);
    var begin = new Date();
    var end = new Date();
    var renewBegin =  new Date();
    var renewEnd =  new Date();
    begin.setDate(begin.getDate() - 7);
    end.setDate(end.getDate() - 1);
    renewBegin.setDate(end.getDate() + 1);
    renewEnd.setDate(end.getDate() + 10);
    this.stamps = {begin: this.datepipe.transform(begin, 'yyyy-MM-dd'), end: this.datepipe.transform(end, 'yyyy-MM-dd')};
    this.renewals = {begin: this.datepipe.transform(renewBegin, 'yyyy-MM-dd'), end: this.datepipe.transform(renewEnd, 'yyyy-MM-dd')};
    this.machines = {begin: this.datepipe.transform(begin, 'yyyy-MM-dd'), end: this.datepipe.transform(end, 'yyyy-MM-dd')};
    this.customers = {begin: this.datepipe.transform(begin, 'yyyy-MM-dd'), end: this.datepipe.transform(end, 'yyyy-MM-dd')};
    this.dealers = {begin: this.datepipe.transform(begin, 'yyyy-MM-dd'), end: this.datepipe.transform(end, 'yyyy-MM-dd')};    
    this.customer_analytic = {begin: this.datepipe.transform(begin, 'yyyy-MM-dd'), end: this.datepipe.transform(end, 'yyyy-MM-dd')};    
    this.service_analytic = {begin: this.datepipe.transform(begin, 'yyyy-MM-dd'), end: this.datepipe.transform(end, 'yyyy-MM-dd')};  
    this.stamp_sd = this.datepipe.transform(begin, 'yyyy-MM-dd');
    this.stamp_ed = this.datepipe.transform(end, 'yyyy-MM-dd');
    this.renew_sd = this.datepipe.transform(renewBegin, 'yyyy-MM-dd');
    this.renew_ed = this.datepipe.transform(renewEnd, 'yyyy-MM-dd');
    this.mach_sd = this.datepipe.transform(begin, 'yyyy-MM-dd');
    this.mach_ed = this.datepipe.transform(end, 'yyyy-MM-dd');
    this.cus_sd = this.datepipe.transform(begin, 'yyyy-MM-dd');
    this.cus_ed = this.datepipe.transform(end, 'yyyy-MM-dd');
    this.dealer_sd = this.datepipe.transform(begin, 'yyyy-MM-dd');
    this.dealer_ed = this.datepipe.transform(end, 'yyyy-MM-dd');
    this.analytics_sd = this.datepipe.transform(begin, 'yyyy-MM-dd');
    this.analytics_ed = this.datepipe.transform(end, 'yyyy-MM-dd');
    this.service_analytics_sd = this.datepipe.transform(begin, 'yyyy-MM-dd');
    this.service_analytics_ed = this.datepipe.transform(end, 'yyyy-MM-dd');
    this.getDetails();
    this.getServiceStatus();
  }
  
  stamping(event) {
    this.stamp_sd = this.datepipe.transform(event.begin, 'yyyy-MM-dd');
    this.stamp_ed = this.datepipe.transform(event.end, 'yyyy-MM-dd');
    this.getDetails();
  }
  renewal(event) {
    this.renew_sd = this.datepipe.transform(event.begin, 'yyyy-MM-dd');
    this.renew_ed = this.datepipe.transform(event.end, 'yyyy-MM-dd');
    this.getDetails();
  }
  machine(event) {
    this.mach_sd = this.datepipe.transform(event.begin, 'yyyy-MM-dd');
    this.mach_ed = this.datepipe.transform(event.end, 'yyyy-MM-dd');
    this.getDetails();
  }
  customer(event) {
    this.cus_sd = this.datepipe.transform(event.begin, 'yyyy-MM-dd');
    this.cus_ed = this.datepipe.transform(event.end, 'yyyy-MM-dd');
    this.getDetails();
  }
  dealer(event) {
    this.dealer_sd = this.datepipe.transform(event.begin, 'yyyy-MM-dd');
    this.dealer_ed = this.datepipe.transform(event.end, 'yyyy-MM-dd');
    this.getDetails();
  }
  cus_analytics(event) {
    this.analytics_sd = this.datepipe.transform(event.begin, 'yyyy-MM-dd');
    this.analytics_ed = this.datepipe.transform(event.end, 'yyyy-MM-dd');
    this.getDetails();
  }
  service_analytics(event) {
    this.service_analytics_sd = this.datepipe.transform(event.begin, 'yyyy-MM-dd');
    this.service_analytics_ed = this.datepipe.transform(event.end, 'yyyy-MM-dd');
    this.getServiceStatus();
  }

  getDetails() {
    this.dashboard.admin_analytics(this.stamp_sd,this.stamp_ed,this.renew_sd,this.renew_ed,this.mach_sd,
      this.mach_ed,this.cus_sd,this.cus_ed,this.dealer_sd,this.dealer_ed,this.analytics_sd,this.analytics_ed).pipe(untilDestroyed(this)).subscribe(res => {
      this.sharedService.setLoaderShownProperty(false);
      this.analytics = res;
      this.customerChart = {
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
        },
        title: {
          text: 'Customer Analytics',
          style: {
            fontSize: '16px' 
         }
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: false
            },
            showInLegend: true
          }
        },
        colors: ['#7f63f4', '#DADDFC'],
        series: [{
          name: 'Brands',
          colorByPoint: true,
          data: [{
            name: 'Successful Customers',
            y: this.analytics.successfull_customers
          }, {
            name: 'Un-Successfull Customers',
            y: this.analytics.unsuccessfull_customers
          }]
        }],
        credits: {
          enabled: false
        },
      }
    }, error => {
      this.toast.error('Something Went Wrong');
    });   
  }

  getServiceStatus() {
    this.dashboard.service_analytics(this.service_analytic.begin, this.service_analytic.end).pipe(untilDestroyed(this)).subscribe(res => {
      this.service = res;
      this.serviceChart = {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Service Status Analytics',
          style: {
            fontSize: '16px' 
         }
        },
        xAxis: {
          categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
          ],
          crosshair: true
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Service'
          }
        },
        tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.f}</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
        },
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0,
            groupPadding: 0,
            shadow: false
          }
        },
        colors: ['#DADDFC', '#7f63f4', '#515151', '#666666'],
        series: [
          {
            name: 'Open',
            data: this.service.Open
          },
          {
            name: 'Inprogress',
            data: this.service["In-progress"]
          },
          {
            name: 'Won',
            data: this.service.Won
          },
          {
            name: 'Lost',
            data: this.service.Lost
          }
        ],
        credits: {
          enabled: false
        },
      }
    }, error => {
      // this.toast.error('Something Went Wrong');
    });
  }

  ngOnDestroy() { }
}
