import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { MatTableModule,MatNativeDateModule,MatSelectModule,MatDatepickerModule,MatIconModule,MatButtonModule,MatCheckboxModule, MatToolbarModule, MatCardModule,MatFormFieldModule,MatInputModule,MatRadioModule,MatListModule,MatGridListModule,MatDividerModule,MatTabsModule, MatTooltipModule, MatSortModule, MatSlideToggleModule, MatProgressBarModule, MatSidenavModule, MatExpansionModule} from '@angular/material';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MatDialogModule } from '@angular/material/dialog'; 
import { ToastrModule } from 'ngx-toastr';
import { DatePipe } from '@angular/common'
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { HighchartsChartModule } from 'highcharts-angular';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';
import { OwlModule } from 'ngx-owl-carousel';







@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatSelectModule,
    MatDatepickerModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatListModule,
    MatDialogModule,
    MatGridListModule,
    MatDividerModule,
    MatTabsModule,
    MaterialFileInputModule,
    FormsModule,
    MatTableModule,
    InfiniteScrollModule,
    MatTooltipModule,
    MatSortModule,
    MatSlideToggleModule,
    MatProgressBarModule,
    HighchartsChartModule,
    MatSidenavModule,
    MatExpansionModule,
    SatDatepickerModule,
    SatNativeDateModule,
    OwlModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-center'
    })
  ],
  exports: [
    ReactiveFormsModule,
    MatNativeDateModule,
    MatSelectModule,
    MatDatepickerModule,
    MatIconModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatListModule,
    MatDialogModule,
    MatGridListModule,
    MatDividerModule,
    MatTabsModule,
    MaterialFileInputModule,
    FormsModule,
    MatTableModule,
    MatTooltipModule,
    InfiniteScrollModule,
    MatSortModule,
    MatSlideToggleModule,
    MatProgressBarModule,
    HighchartsChartModule,
    MatSidenavModule ,
    MatExpansionModule,
    SatDatepickerModule,
    SatNativeDateModule,
    OwlModule
],
providers: [DatePipe]
})
export class SharedModule { }
